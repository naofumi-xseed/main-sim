<?php
include "../_config/db.php";

session_start();

$time = date('Y-m-d H:i:s');


@$username = $_SESSION['username'];

#get all current user details

$sql_user = "SELECT id from payco_users where username='{$username}'";
$result = $db->query($sql_user);

if($result->num_rows < 1){
  echo "<script>
  window.location='../';
  </script>
  ";
  session_destroy();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Font awesome icons -->
  <link rel="stylesheet" href="../_public/css/allv2.css">
  <!-- Css -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../_public/css/efrolic.min.css">
  <link rel="stylesheet" href="../_public/css/payco.css">
  <link rel="stylesheet" href="../_public/css/Chart.css">



  <!-- Javascript -->
  <script src="../_public/js/jquery-3.4.0.js"></script>
  <title>PayCo</title>
</head>

<body ng-app="paycoApp" ng-controller="mainController" ng-cloak>
  <div class="e-container-fluid" ng-init="getActiveUser()">
    <nav class="e-nav blue-gradient text-white">
      <!-- dark-blue -->
      <img src="../_public/photos/logo.png" style="width:90px;height:auto;">
      <span>
        <h6 class="text-white no-m">Midway Colleges</h6>
        <p class="size-7 text-white no-m">School Information Management Portal</p>
      </span>
      <label for="e-menu" class="e-btn small circle inverted no-desktop"><i class="fas fa-bars"></i></label>
      <input type="checkbox" id="e-menu">
      <div class="e-menu between px-3">
        <i class="e-distribution"></i>

        <div class="e-dropdown text-white">
          <h6 class="">
            <i class="fas fa-chart-bar"></i>
            Reports <i class="fas fa-angle-down"></i>
          </h6>
          <div class="drop-items">
            <a class="drop-item" href="?end-of-day">End of Day/Clearing</a>
            <a class="drop-item" href="?daily-collection">Daily Collection</a>
            <a class="drop-item" href="?summary-collection">Summary Collection</a>
            <hr>
            <a class="drop-item" href="?apprentice-fee">Apprentice Fees</a>
            <a class="drop-item" href="?graduation-fee">Graduation Fees</a>
          </div>
        </div>

        <div class="e-dropdown text-white">
          <h6 class="">
            <i class="fas fa-user-cog"></i>
            Setup <i class="fas fa-angle-down"></i>
          </h6>
          <div class="drop-items">
            <a class="drop-item" href="?customers">Customers</a>
            <a class="drop-item" href="?fees">Fees</a>
            <a class="drop-item" href="?category">Category</a>
            <a class="drop-item" href="?receipts">Receipts</a>
            <a class="drop-item" href="?users">Users</a>
          </div>
        </div>
        <h6><a class="e-menu-item text-white" href="?cashboard"><i class="fas fa-money-check"></i> Cashboard</a></h6>
        <h6><a class="e-menu-item text-white"><i class="fas fa-check-square"></i> Validate</a></h6>



        <i class="e-distribution"></i>
        <span class="px-3">
          <span class="text-center e-title no-p">PayCo</span>
          <br>

          <span class="no-p">
            Hi! <strong><a href="" >{{activeCredentials.username}}</a></strong>

            <a href="?logout" class="pl-2 text-white"><i class="fas fa-sign-out-alt"></i> logout</a>
          </span>
        </span>

      </div>
    </nav>

    <?php
if ($_SERVER['QUERY_STRING'] == ""){
    include './views/landingpage.html';
}
if(isset($_GET['logout'])){

    echo "<script>
    window.location='../';
    </script>
    ";
    session_destroy();

}

if(isset($_GET['customers'])){
    if(isset($_GET['id'])){
        include './views/customerTransactionLedger.php';
    }else{
        include './views/customer.html';
    }
}


if(isset($_GET['end-of-day'])){
   include './views/endofday.html';
}
if(isset($_GET['daily-collection'])){
   include './views/dailycollection.html';
}
if(isset($_GET['summary-collection'])){
  include './views/summaryCollection.html';
}
elseif(isset($_GET['category'])){
    include './views/categories.html';
}
elseif(isset($_GET['users'])){
    include './views/users.html';
}
elseif(isset($_GET['receipts'])){
    include './views/receipt.html';
}
elseif(isset($_GET['fees'])){
    include './views/fees.html';
}elseif(isset($_GET['cashboard'])){
    include './views/cashboard.html';
}elseif(isset($_GET['apprentice-fee'])){
  include './views/apprenticeFee.html';
}elseif(isset($_GET['graduation-fee'])){
  include './views/graduationFee.html';
}

?>


    <div class="e-footer-bar primary">
      <p>©<a class="link">2019 Midway Colleges Inc.</a> | Created and Developed by IT Department | All rights reserved
      </p>
    </div>


<!-- receipt description modal -->
    <div class="e-modal" id="receiptDetails">
      <div class="e-modal-content eSlide">
            <header class="e-modal-header">
                  <p class="e-modal-title">
                  OR #  {{receiptDetails.receipt_no}} 
                  <span class='e-tag purple-gradient size-7 small' ng-bind="(receiptDetails.is_posted == 1) ? 'posted' : 'not posted'"></span>
                  <span class='e-tag blue-gradient size-7 small'>  {{receiptDetails.description}}  </span>
                    <span class='e-tag small black small size-7' ng-bind="(receiptDetails.is_voided == 1) ? 'voided' : 'not voided'"></span>
                  </p>
                  <button type="button" class="e-delete close">
                        <i aria-hidden="true">&times;</i>
                  </button>
            </header>
            <div class="e-modal-body">
                  <div class="e-cols">
                        <div class="e-col-6" id="contents">
                        <p><strong>Student Number: #</strong> {{receiptDetails.student_number}}</p>
                        <p><strong>Student Name: </strong>{{receiptDetails.last_name}}, {{receiptDetails.first_name}} {{receiptDetails.middle_name}}</p>
                        <p><strong>Cashier: </strong> {{receiptDetails.cashier}}</p>
                        <p><strong>Created at: </strong> {{receiptDetails.created_at}} </p>

                        </div>
                        <div class="e-col-6" id="bill">
                              <div class="e-list">
                                    <div class="marked p-3">
                                          Payment Transaction
                                    </div>

                                    <a class="e-list-item">
                                         <span class="e-tag primary">@Breakdown</span> 
                                    </a>
                                    <a class="e-list-item" ng-repeat="pb in paymentBreakdown">
                                    <strong>Description: </strong> {{pb.code}} - {{pb.description}}<strong>  : </strong>₱ {{pb.amount | number:2}}
                                    <span class="e-tag static" ng-bind='(pb.is_posted == 1) ? "posted" : "not posted"'></span> 
                                    <span class="e-tag static" ng-bind='(pb.is_voided == 1) ? "voided" : "not voided"'></span>
                                    </a>
                                    <a class="e-list-item">
                                    <strong>Cash: </strong> ₱{{receiptDetails.cash | number:2}}
                                    </a>
                                    <a class="e-list-item">
                                    <strong>Change: </strong> ₱{{receiptDetails.cash_change | number:2}}
                                    </a>
                                    <a class="e-list-item">
                                    <strong>Amount Paid: </strong> ₱{{receiptDetails.subtotal | number:2}}
                                    </a>
                              </div>
                        </div>

                  </div>
            </div>

            <div class="e-modal-footer">
                  <button class="e-btn purple close">Close</button>
                  <!-- <button class="e-btn purple">Save changes</button> -->
            </div>
      </div>
</div>


<script>
$(function() {
$('input[name="daterange"]').daterangepicker();
});

function showReceiptDetails(){
      $('#receiptDetails').addClass("launch");
}

$('.close').click(function () {
      $('.e-modal').removeClass('launch');
});
</script>

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
    <script src="../_public/js/angular.this.js"></script>
    <script src="../_public/js/Chart.js"></script>
    <script src="../_public/js/sweetalert.min.js"></script>
    <script src="../_public/js/moment.min.js"></script>
    <script src="../_public/js/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../_public/css/daterangepicker.css">


    <script src="controllers/checklist-model.js"></script>
    <script src="controllers/angular-chart.js"></script>
    <script src="controllers/dirPaginate.js"></script>
    <script src="controllers/app.js"></script>

    </div>
</body>

</html>
