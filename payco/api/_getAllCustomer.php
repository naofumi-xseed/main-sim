<?php

include "../../_config/db.php";

$output = array();  
$query = $db->query("SELECT DISTINCT p.id,enrollment_student.id as enrollment_id, p.student_number,p.first_name,p.middle_name,p.last_name,fees_summary,soc.name
 FROM enrollment_student 
 INNER JOIN preregistration_info p ON p.id = student_id 
 INNER JOIN strands_courses soc ON soc.id = strand_id 
 WHERE enrollment_student.id
 IN (SELECT max(enrollment_student.id) as enrollment_id 
 FROM enrollment_student GROUP by student_id ORDER by enrollment_id DESC)
");  
 
     while($row = $query->fetch_assoc())  
     {  
         $output[]= $row;  
     }  
     
$json  = json_encode($output, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;

     echo $json;
 

?>

