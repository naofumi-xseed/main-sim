<?php

include "../../_config/db.php";

$output = array();  
$query = $db->query("SELECT pt.id as receipt_id,r.id,r.receipt_no,r.status,r.remarks,users.name FROM `payco_receipts`as r 
LEFT OUTER JOIN payco_users as users on users.id = `payco_user_id` 
LEFT OUTER JOIN payco_transactions as pt on pt.receipt_no = r.receipt_no ORDER BY r.id DESC
");  
 
     while($row = $query->fetch_assoc())  
     {  
         $output[]= $row;  
     }  
     
$json  = json_encode($output, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;

     echo $json;
 

?>