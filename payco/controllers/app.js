var payco = angular.module('paycoApp', ['angularUtils.directives.dirPagination','chart.js','checklist-model'])

.controller('mainController', function ($scope, $http, $compile) {
// SORTING
$scope.propertyName = 'last_name';
$scope.reverse = true;

$scope.sortBy = function(propertyName) {
  $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
  $scope.propertyName = propertyName;
};

//===============================================
//utils
$scope.rangeLoop = function(item){
    console.log(item);
    alert(item);
}
//===============================================

$scope.date = new Date();
$scope.today = 
$scope.date.getFullYear() + '/' +
($scope.date.getMonth() + 1) + '/' +
$scope.date.getDate();

$scope.colorsTuition = ["#00C9FF", "#343a40", "#512DA8"];

$scope.getActiveUser = function(){
    $http.get('./api/_getSessionUser.php')
         .then(response => {
            $scope.activeCredentials = response.data;
            //get available receipts of user
            $http.get('./api/_getAvailableReceipt.php?userID=' +  $scope.activeCredentials.id)
            .then(response => {
                $scope.availableReceipts = response.data;
            })
         });
};
//settings
$scope.getCurrentSettings = function () {
    $http.get('./api/_getCurrentSettings.php')
        .then(response => {
            $scope.currentSettings = response.data;
        });
};

$http.get('./api/_getNotCurrentSettings.php')
.then(response => {
    $scope.notCurrentSettings = response.data;
    
});

$scope.customerID;
$scope.settingID;

$scope.dataTuition  = [[100],[100],[100]];
$scope.labelsTuition = ["SEARCH FOR A CUSTOMER FIRST"]
$scope.seriesTuition = ["1","2","3"]

$scope.dataBackAccount = [1000,700,400];
$scope.labelsBackAccount = ["SEARCH FOR A CUSTOMER FIRST", "SEARCH FOR A CUSTOMER FIRST","SEARCH FOR A CUSTOMER FIRST"];

$scope._tuitionPaid;
$scope._tuitionPaidPercent;
$scope._tuitionBalance;
$scope._tuition40;
$scope._tuition70;
$scope._tuition100;

$scope._backAccountPaidPercent;
$scope._backAccountPaid;
$scope._backAccountRemaining;
$scope._backAccountTotal;

$scope.getCustomerByID = function(){
     $http.get('./api/_getCustomerByID.php?id=' + $scope.customerID + '&sid=' + $scope.settingID)
         .then(response => {
             if(response.data.length < 1){
                 swal('Customer not enrolled (on this selected setting)',"Kindly enter the correct data on settings/student number input box", "warning");
             }else{
                var theCustomer = response.data;
                theCustomer.map((item,index) => {
                    theCustomer[index].fees_summary = JSON.parse(item.fees_summary);

                })
             $scope.theCustomer = theCustomer;
    
             $http.get('./api/_getFeesSummaryJson.php?id=' + $scope.theCustomer[0].enrollment_id)
                 .then(response => {
                     $scope.FeesSummary = JSON.parse(JSON.parse(response.data));
                     var data = JSON.parse(JSON.parse(response.data));

                     //needed on customer tags below the bar graph [legend]
                    //  $scope._tuitionPaid = data.tuition_summary[2].slice(-1)[0];
                    //  $scope._tuitionPaidPercent = (data.tuition_summary[2].slice(-1)[0]
                    //                              / data.tuition_summary[0].slice(-1)[0]) * 100;
                    //  $scope._tuitionBalance = data.tuition_summary[1].slice(-1)[0];

                    //  $scope._tuition40 = ($scope._tuitionPaidPercent >= 40) ? 0.00 :((data.tuition_summary[0].slice(-1)[0] * 0.4) 
                    //                      - data.tuition_summary[2].slice(-1)[0]);

                    //  $scope._tuition70 = ($scope._tuitionPaidPercent >= 70) ? 0.00 : (data.tuition_summary[0].slice(-1)[0] * 0.7)
                    //      - data.tuition_summary[2].slice(-1)[0];

                    //  $scope._tuition100 = (data.tuition_summary[0].slice(-1)[0] * 1)
                    //      - data.tuition_summary[2].slice(-1)[0];

                     //END needed on customer tags below the bar graph [legend]

                        console.log(response.data);
                        
                    //  $scope.dataTuition = data.tuition_summary;
                    //  $scope.labelsTuition = data.tuition_payment_timestamps;
                    //  $scope.seriesTuition = ['Total Tuition Fee', 'Balance Remaining', 'Tuition Fee Paid'];
                 });
                 
            //get back accounts
            $http.get('./api/_getBackAccountsSummaryJson.php?id=' + $scope.customerID)
                .then(response => {
                    // $scope.backAccountSummary = JSON.parse(JSON.parse(response.data));
                  
                    
                    // var data = JSON.parse(JSON.parse(response.data));

                     //needed on customer tags below the pie chart [legend]
                    // $scope._backAccountPaidPercent = (data.back_accounts_summary[2]/data.back_accounts_summary[0]) * 100;
                    // $scope._backAccountPaid = data.back_accounts_summary[2];
                    // $scope._backAccountRemaining = data.back_accounts_summary[1];
                    // $scope._backAccountTotal = data.back_accounts_summary[0];
                    // $scope.dataBackAccount = data.back_accounts_summary;
                    // $scope.labelsBackAccount = ['Total Accumulated Back Account', 'Back Accounts Remaining', 'Back Accounts Paid'];


                    //
                    console.log(response.data);
                    $scope.student_ba = response.data;
                    
                });


                // get consumedsetting
                $http.get('./api/_getAllSettingsOfStudentBySN.php?id=' + $scope.customerID)
                .then(response => {
                    $scope.userConsumedSettings = response.data;
                    console.log('consumed setting');
                    
                });


             }
         });

};

$scope.getAllFeesByID = function(code){

    $http.get('./api/_getFeesByID.php?code='+ code)
         .then(response =>{
             $scope.allFees = response.data;
         });
        $scope.buttonClicked = code;
};

$scope.amountFees = parseFloat(0);
$scope.transaction_details = [];
$scope.cashReceived;
$scope.change = parseFloat(0);
$scope.id;
$scope.code;
$scope.description;
$scope.amount;
//for tf and ba
$scope.addTransaction2 = function () {
    $scope.transaction_details.push([$scope.id, $scope.code, $scope.description, $scope.amount, undefined]);
    $scope.amountFees += parseFloat($scope.amount);
    console.log($scope.transaction_details);

}

$scope.addTransactionBackAccounts = function (fee_id,code,desc,amount,ba_id) {
    $scope.transaction_details.push([fee_id, code, desc, amount, undefined, ba_id]);
    $scope.amountFees += parseFloat(amount);
    console.log($scope.transaction_details);
    // console.log('ba_id');
}


$scope.addTransaction = function(id,code,description,amount,batch_id){
    $scope.transaction_details.push([id,code,description,amount,batch_id]);
     $scope.amountFees += parseFloat(amount);
     console.log($scope.transaction_details);
     
}
$scope.dismiss = function (id,amount) {
    $scope.transaction_details.splice(id,1);
    $scope.amountFees = $scope.amountFees-amount;
}


//Pay now functions
$scope.payNow = function(){
//receipt number initialize

    if ($scope.transaction_details == [] || $scope.cashReceived == 0 ||
         $scope.cashReceived == null || $scope.customerID == null || $scope.settingID == null ||
         $scope.customerID == 0 || $scope.settingID == 0 || $scope.receiptSelected == undefined || $scope.receiptSelected == null || $scope.receiptSelected == 0) {
            swal("Please enter all the data needed to proceed!", "Please check all input box or check if there is a payable selected....", "warning");
    }else{
    $scope.processedDetails=[];
    var receipt_no = JSON.parse($scope.receiptSelected).receipt_no;
    var receipt_id = JSON.parse($scope.receiptSelected).id;



    $scope.transaction_details.forEach(fee => {
        if (fee[4] == undefined || fee[4] == null || fee[4] == 0) {
        var details = {'fee_id'     : fee[0],
                      'code'        : fee[1],
                      'description' : fee[2],
                      'amount'      : fee[3],
                      'is_posted'   : 1,
                      'is_voided'   : 0,
                     };
            
            $scope.processedDetails.push(details);
            if (fee[1] == 'BA'){
            // UPDATING Back_accounts_summary IN ENROLLMENT_STUDENT FOR BA ONLY
            var backaccountPaid = $scope.backAccountSummary.back_accounts_summary[2] + parseFloat(fee[3]);
            var backaccountRemaing = $scope.backAccountSummary.back_accounts_summary[0] - backaccountPaid;
            var theBackAccountSummary = [];
            theBackAccountSummary.push($scope.backAccountSummary.back_accounts_summary[0], backaccountRemaing, backaccountPaid);

            $scope.backAccountSummary.back_accounts_summary = theBackAccountSummary;

            $http.post('./api/_updateBackAccountSummaryJson.php', {
                'backAccountSummary': JSON.stringify($scope.backAccountSummary),
                'id': $scope.theCustomer[0].enrollment_id
            }).then(response => {
                console.log(response.data.status);
            });


            }else if (fee[1] == 'TF'){

                // UPDATING FeesSummary IN ENROLLMENT_STUDENT FOR TF ONLY
                var tuitionPaid = $scope.FeesSummary.tuition_summary[2].slice(-1)[0];

                $scope.FeesSummary.tuition_summary[0].push($scope.FeesSummary.total_tuition_fee);

                $scope.FeesSummary.tuition_summary[1].push(($scope.FeesSummary.total_tuition_fee - (tuitionPaid)) - parseFloat(fee[3]));
                $scope.FeesSummary.tuition_summary[2].push(tuitionPaid + parseFloat(fee[3]));

                $scope.FeesSummary.tuition_payment_timestamps
                    .push(
                        $scope.date.getMonth() + 1 + '/' +
                        $scope.date.getDate() + '/' +
                        $scope.date.getFullYear()
                    );

                $http.post('./api/_updateFeesSummaryJson.php', {
                    'feesSummary': JSON.stringify($scope.FeesSummary),
                    'id': $scope.theCustomer[0].enrollment_id
                }).then(response => {
                    console.log(response.data.status);
                });
                //END UPDATING FeesSummary IN ENROLLMENT_STUDENT FOR TF ONLY


            }
//END UPDATING Back_accounts_summary IN ENROLLMENT_STUDENT FOR BA ONLY
            if (fee[1] == 'TF' || fee[1] == 'BA' ){
                $http.post('./api/_insertPaymentTransactions.php',
                    {
                        'subtotal': fee[3],
                        'receipt_no':  receipt_no,
                        'cashier': $scope.activeCredentials.name,
                        'enrollment_id': $scope.theCustomer[0].enrollment_id,
                        'transaction_details': JSON.stringify(details)
                    })
                    .then(response => {
                        console.log(response.data.status);
                    });
            }
        }else{
            $http.post('./api/_insertBatchDetails.php',
                {
                    'batch_id': fee[4],
                    'enrollment_id': $scope.theCustomer[0].enrollment_id
                })
                .then(response => {
                    console.log(response.data.status);
                });
            var details = {'fee_id'     : fee[0],
                      'code'        : fee[1],
                      'description' : fee[2],
                      'amount'      : fee[3],
                      'is_posted'   : 1,
                      'is_voided'   : 0,
                     };
            
            $scope.processedDetails.push(details);
        }

    });

    $scope.change = ($scope.cashReceived - $scope.amountFees);
    $http.post('./api/_insertTransactions.php',
                { 'customer_number' : $scope.customerID,
                   'settingID'      : $scope.settingID,
                   'subtotal'       : $scope.amountFees,
                   'cash'           : $scope.cashReceived,
                   'change'         : $scope.change,
                   'receipt_no'     : receipt_no,
                    'enrollment_id': $scope.theCustomer[0].enrollment_id,
                   'transaction_details': JSON.stringify($scope.processedDetails)
                   })
          .then(response => {
              swal( response.data.status +"  The customer's change is Php " + $scope.change, "", "success");

              $scope.transaction_details = [];
              $scope.amountFees = parseFloat(0);
              $scope.cashReceived = parseFloat(0);
              $scope.change = parseFloat(0);
              $scope.getCustomerByID();
          });

    //mark receipt as used
        $http.post('./api/_updateStatusReceiptByID.php', {
            'id': receipt_id
        }).then(response =>{
                console.log(response.data.msg,response.data.p,response.data.type);
                $scope.getActiveUser();
            });
}
}


//===========================receipt controller==========================
$scope.from;
$scope.to;
$scope.getReceipts = function (){
    $http.get('./api/_getReceipts.php')
    .then(response => {
        $scope.receipts = response.data;
    })
}

$scope.generateOR = function(){

    $http.post('./api/_generateOR.php', {
        'from': $scope.from,
        'to': $scope.to,
        'cashier': $scope.activeCredentials.id
    }).then(response =>{
            swal('Receipt Generated From ' + $scope.from + ' to ' + $scope.to + " Successful!","","success");
            $scope.getReceipts();
        });
}
$scope.receipt_no;

$scope.voidReceipt = function(id){
    $http.post('./api/_voidReceipt.php',{
        'receipt_no': id
    }).then(response => {
        swal(response.data.msg,response.data.p,response.data.type);
        // console.log(response);
        
        $scope.getReceipts();

        
    })
}


//===========================user controller==========================
$scope.initial_roles = ["cashier","admin"];
$scope.userRoles = {
roles: ["cashier"]
};


$scope.nFname;
$scope.nPw;
$scope.nEmail;
$scope.nUsername;
$scope.nID;

$scope.action;

$scope.addUser = function(){
    $http.post('./api/_insertUser.php',{
        'fname' : $scope.nFname,
        'pw' : $scope.nPw,
        'email' : $scope.nEmail,
        'username' : $scope.nUsername,
        'roles' : JSON.stringify($scope.userRoles.roles)
    }).then(response => {
        swal(response.data.msg,response.data.p,response.data.type);
        $scope.getAllUsers();

    });
}

$scope.updateUser = function(){
    $http.post('./api/_updateUser.php',{
        'fname' : $scope.nFname,
        'pw' : $scope.nPw,
        'email' : $scope.nEmail,
        'username' : $scope.nUsername,
        'roles' : JSON.stringify($scope.userRoles.roles),
        'id' : $scope.nID
    }).then(response => {
        swal(response.data.msg,response.data.p,response.data.type);
        $scope.getAllUsers();
        console.log(response.data);
        
    });
}

$scope.getAllUsers =  function () {
    $scope.users=[];
    $http.get('./api/_getAllUsers.php')
         .then(response => {
             var user = response.data;
             user.forEach((item,index) => {
                user[index].roles = JSON.parse(item.roles);
                $scope.users.push(item);
             });
                           
         });
}

$scope.getUserByID =  function () {
    //get user by id 
    $http.get('./api/_getUserByID.php?id=' + $scope.nID)
    .then(response => {
        $scope.nPw = '';
        $scope.nFname = response.data.name;
        $scope.nEmail = response.data.email;
        $scope.nUsername = response.data.username;
        $scope.userRoles.roles = JSON.parse(response.data.roles); 
    });
};

$scope.activateUser =  function (id,to) {
    $http.post('./api/_activateUser.php?',{
                    "id" : id,
                    "to" : to
                }
            )
         .then(response => {
             console.log(response);
             
             swal(response.data.msg,"",response.data.type);
             $scope.getAllUsers();
        });
}

$scope.userController = function(action,id){
    $scope.nID = id;
    if(action == 'update'){
        $scope.action = 'update';
       $scope.getUserByID();

    }else if(action == 'add'){
        $scope.action = 'add';
        $scope.nFname = '';
        $scope.nPw = '';
        $scope.nEmail = '';
        $scope.nUsername = '';
        $scope.nID = '';
    }
}
//===========================fees controller==========================
$scope.fCategoryID = '';
$scope.fDescription = '';
$scope.fAmount = '';
$scope.fCode = '';


$scope.getFees = function (){
    $http.get('./api/_getFees.php?catID=' + $scope.fCategoryID)
    .then(response => {
        $scope.fees = response.data;
        console.log($scope.fCategoryID);
        
    })
}
$scope.codeExists = parseInt(0);
$scope.checkCode = function(){
    $http.get('./api/_countFeeCodeByCode.php?code=' + $scope.fCode)
        .then(response => {
            $scope.codeExists = (response.data);
        })
}


$scope.addFees = function(){
    if($scope.codeExists  > 1){
        swal("Code already exists!","try a new code","warning");
        }
    else if( $scope.fCategoryID == '' ||
                  $scope.fDescription == '' ||
                  $scope.fAmount == '' ||
                  $scope.fCode == ''){
        swal("Some fields are empty!","try again when form is all filled up","warning");
    }else{
        $http.post('./api/_insertFee.php?',{
            "code" : $scope.fCode.toUpperCase(),
            "desc" : $scope.fDescription.toUpperCase(),
            "amount": parseFloat($scope.fAmount).toFixed(2),
            "catID" : parseInt($scope.fCategoryID)
                }
            )
            .then(response => {                
                swal(response.data.msg,response.data.p,response.data.type);
                // console.log(response.data);
                $scope.getFees();
                $scope.fCategoryID = '';
                $scope.fDescription = '';
                $scope.fAmount = '';
                $scope.fCode = '';

            });
    }
}

//===========================customer controller==========================
$scope.getCustomers = function (){
    $http.get('./api/_getAllCustomer.php')
    .then(response => {
        var customers = (response.data);        
       customers.map((customer,index) =>{
           customers[index].fees_summary = JSON.parse(customer.fees_summary);
       }) ;
       $scope.customers = customers;
    })
}

$scope.getConsumedSettings = function (params){
    $http.get('./api/_getAllSettingsOfStudentBySN.php?id=' + params)
    .then(response => {
       $scope.consumedSettings = response.data;
       console.log($scope.consumedSettings);
       
    })
}

$scope.getTransactionBySetting = function (setting_id,enrollment_id){
    
    $http.get('./api/_getStudentTransactionBySetting.php?sid=' + setting_id + '&eid=' + enrollment_id)
    .then(response => {
       $scope.transactions = response.data;    
       $scope.total = parseFloat(0);
       
       $scope.transactions.map(t => {
        $scope.total += parseFloat(t.subtotal);
       })
    })
    $scope.buttonClicked = setting_id;
}


$scope.getReceiptDetails = function (id){
    
    $http.get('./api/_getReceiptDescription.php?id=' + id)
    .then(response => {                        
      var details = JSON.parse(response.data.transaction_details);
      $scope.paymentBreakdown = details;
      $scope.receiptDetails = response.data;        
    })
}

//===========================category controller==========================
$scope.categoryType;

$scope.getCategory = function(){
    $http.get('./api/_getCategories.php')
        .then(response =>{
            $scope.categories = response.data;
        })
}


$scope.getCategoryByID = function (id,type) { //used in updating category
    $http.get('./api/_getCategoryByID.php?id=' + id)
    .then(response =>{
        $scope.categoryID = response.data.id;
        $scope.categoryCode = response.data.code;
        $scope.categoryDesc = response.data.description;
        $scope.categoryType = type;
    
    })
}

$scope.updateCategory = function () {
    $http.post('./api/_updateCategory.php',{
        "id" :  $scope.categoryID ,
        "code" :  $scope.categoryCode,
        "desc" : $scope.categoryDesc
    })
    .then(response =>{
        swal(response.data.msg,response.data.p,response.data.type);
        console.log(response.data);
        $scope.getCategory();
    })
}


$scope.addCategory = function () {
    $http.post('./api/_insertCategory.php',{
        "code" : $scope.categoryCode,
        "desc" : $scope.categoryDesc
    })
    .then(response =>{
        swal(response.data.msg,response.data.p,response.data.type);
        $scope.getCategory();
    })


}




//===========================endofday controller==========================

$http.get('./api/_allPaycoUsers.php')
     .then(response => {
         $scope.allPaycoUsers = response.data;
        $scope.allPaycoUsersLength = response.data.length;
     });


$scope.getEndofDay = function (id){
    
    $http.get('./api/_getTransactionsEndofDay.php?userid=' + id)
    .then(response => {
     var endofday = response.data; 
     endofday.map((item,index)=> {
         endofday[index].transaction_details = JSON.parse(item.transaction_details);
     })       
     $scope.endofday = endofday;
     
      //get total of all transactions
      $scope.totalEndofDay = parseFloat(0);
      $scope.endofday.map((t) => {
            t.transaction_details.map(item => {
                $scope.totalEndofDay += parseFloat(item.amount);
            });
       });
      //get total tuition,backaccount,other fess paid per payco users 
      $scope.arrayofFeesComputed = [[],[],[]];
      $scope.totalYComputedHeaders = [];
      $scope.totalXComputedHeaders = [];

      $scope.allPaycoUsers.map((user,index)=> {
          var xTuition = parseFloat(0); //tuition tmp holder
          var xBA = parseFloat(0); //back accounts tmp holder
          var xOF = parseFloat(0); //other fees tmp holder
            $scope.endofday.map(t => {
               if(user.id == t.user_id){
                    t.transaction_details.map(item => {
                        if(item.code == 'TF'){
                            xTuition += parseFloat(item.amount);
                        }else if(item.code == 'BA'){
                            xBA += parseFloat(item.amount);
                        }else{
                            xOF += parseFloat(item.amount);
                        }
                    });
               }
            })
            $scope.arrayofFeesComputed[0].push(xTuition);
            $scope.arrayofFeesComputed[1].push(xBA);
            $scope.arrayofFeesComputed[2].push(xOF);
      });

      var totalYHeader = parseFloat(0); // variable for total y axis header
      $scope.arrayofFeesComputed.map((arr,index) => { // loop through the array by categories (tf,ba,of)
        var yHeader = parseFloat(0);
        arr.map(amount => {                         // loop through the values per categories
            yHeader += parseFloat(amount);
        })
        totalYHeader += yHeader; //set total amount of total y axis header
        $scope.totalYComputedHeaders.push(yHeader); // return value to zero to count another row (tf,ba,of)
      });
      $scope.totalYComputedHeaders.push(totalYHeader); // push including total y axis header


    //   X HEADER COMPUTATION
      $scope.allPaycoUsers.map((user,userindex) => { //loop throuh users to get column
        var xHeader = parseFloat(0);                //set variable to store the sum of each column
        $scope.arrayofFeesComputed.map((arr,index) =>{ //loop through the row selecting the specific column
            xHeader += parseFloat(arr[userindex])            
        })
        $scope.totalXComputedHeaders.push(xHeader); //sum of the computed column
      })

      
      console.log($scope.arrayofFeesComputed);
      console.log($scope.totalYComputedHeaders);
      console.log($scope.totalXComputedHeaders);


      

    })
}


//===========================daily collection controller==========================


$scope.a = function(){
    console.log($scope.dateRange);
}
if(!$scope.dateRange){
    $scope.dateRange = $scope.today + " - " + $scope.today;
}

$scope.selectedUforReports = '%';
$scope.getDailyCollection = function (){
    
    var from = moment($scope.dateRange.split(" - ")[0]).format("YYYY-M-D");
    var to = moment($scope.dateRange.split(" - ")[1]).add(1,'d').format("YYYY-M-D");
    var dateRange = from + " , " + to;

    $scope.displayDateRange = from + ' to '+  moment($scope.dateRange.split(" - ")[1]).format("YYYY-M-D");

    $http.get('./api/_getDailyCollection.php?userid=' + $scope.selectedUforReports + '&dateRange=' + dateRange)
    .then(response => {
        console.log(response.data);
        
        
     var dailycollection = response.data; 
     dailycollection.map((item,index)=> {
        dailycollection[index].transaction_details = JSON.parse(item.transaction_details);
     })       
     $scope.dailycollection = dailycollection;
     
      //get total of all transactions
      $scope.totaldailycollection = parseFloat(0);
 
      $scope.dailycollection.map((t) => {
            t.transaction_details.map(item => {
                $scope.totaldailycollection += parseFloat(item.amount);
            })
       })


    })
}

//===========================summary collection controller==========================

$scope.getSummary = function (){
    
    var payload = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    var payload_of= [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    var payload_bf= [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    var payload_af= [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
    var payload_ba = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];

    
    var summaryHeaders = [[],[],[],[],[],[]];
    var tmpsummaryHeaders = [];
    var from = moment($scope.dateRange.split(" - ")[0]).format("YYYY-M-D");
    var to = moment($scope.dateRange.split(" - ")[1]).add(1,'d').format("YYYY-M-D");
    var dateRange = from + " , " + to;

    $scope.displayDateRange = from + ' to '+  moment($scope.dateRange.split(" - ")[1]).format("YYYY-M-D");

    $http.get('./api/_getSummaryCollection.php?userid=' + $scope.selectedUforReports + '&dateRange=' + dateRange)
    .then(response => {

         let summary = response.data;
         
         summary.map((item,index) => { //iterate per user
            let temp_td = [];               //temporary transaction details holder 
            item.map(t => {           //get all transaction
                let items = JSON.parse(t); //transform into json
                temp_td.push(items);        //push object to a temporary container (contains transaction of user)
                items.map(i => {
                    tmpsummaryHeaders.push(i.code + ' - ' + i.description);
                }); // get all the headers

             })

            temp_td = _.flatten(temp_td);
            summary[index] = _.groupBy(temp_td, 'code');

         })
         tmpsummaryHeaders = _.uniq(tmpsummaryHeaders); //headers must be uniq
         tmpsummaryHeaders.map(header => {
             let tmpCode = header.split(" - ")[0].slice(0,2);
             if(tmpCode == 'TF'){
                 summaryHeaders[0].push(header);
             }else if(tmpCode == 'AF'){
                summaryHeaders[1].push(header);
             }else if(tmpCode == 'BF'){
                summaryHeaders[2].push(header);
             }
             else if(tmpCode == 'OF'){
                summaryHeaders[3].push(header);
             }else if(tmpCode == 'BA'){
                summaryHeaders[4].push(header);
             }
         })

         //creation of payload for tuition fees
         summaryHeaders[0].map((header,i) => {
            let tmpCode = header.split(" - ")[0];
            payload[i].push(header);
            summary.map(items => {

                if(! _.isEmpty(items)){
                    items = Object.entries(items);
                    var xvalue = parseFloat(0.00);
                    for([code,value] of items){
                        
                        if (code == tmpCode){
                            value.forEach(v => {
                                xvalue += parseFloat(v.amount); // push the value
                            })
                        }
                    }
                    payload[i].push(xvalue);
                }else{
                    payload[i].push(0.00);
                }
            })
         })
         
            //creation of payload for apprentice fees
            summaryHeaders[1].map((header,i) => {  //summaryHeaders for other fees. this gets all other fees transactions on the selected date
                let tmpCode = header.split(" - ")[0];   //get the code of each transactions
                payload_af[i].push(header);             //include the entire header in the payload variable
                
                summary.map((items,index) => {                //loop through the summary array (per user)
                    if(!_.isEmpty(items)){            //if user have transactions
                        var new_items = Object.entries(items);
                        var xvalue = parseFloat(0.00);
                        for([code,value] of new_items){ //loop through the transaction
                            if(code == tmpCode){        //check if the transactions made matches the code on the summary header
                                value.forEach(v => {
                                    xvalue += parseFloat(v.amount); // push the value
                                })
                            }
                        }
                        payload_af[i].push(xvalue);
                    }else{
                        payload_af[i].push(0.00);
                    }
    
                })
                })
              

            //creation of payload for Book fees
            summaryHeaders[2].map((header,i) => {  //summaryHeaders for other fees. this gets all other fees transactions on the selected date
            let tmpCode = header.split(" - ")[0];   //get the code of each transactions
            payload_bf[i].push(header);             //include the entire header in the payload variable
            
            summary.map((items,index) => {                //loop through the summary array (per user)
                if(!_.isEmpty(items)){            //if user have transactions
                    var new_items = Object.entries(items);
                    var xvalue = parseFloat(0.00);
                    for([code,value] of new_items){ //loop through the transaction
                        if(code == tmpCode){        //check if the transactions made matches the code on the summary header
                            value.forEach(v => {
                                xvalue += parseFloat(v.amount); // push the value
                            })
                        }
                    }
                    payload_bf[i].push(xvalue);
                }else{
                    payload_bf[i].push(0.00);
                }

            })
            })
         //creation of payload for other fees
         summaryHeaders[3].map((header,i) => {  //summaryHeaders for other fees. this gets all other fees transactions on the selected date
            let tmpCode = header.split(" - ")[0];   //get the code of each transactions
            payload_of[i].push(header);             //include the entire header in the payload variable
            
            summary.map((items,index) => {                //loop through the summary array (per user)
                if(!_.isEmpty(items)){            //if user have transactions
                    var new_items = Object.entries(items);
                    var xvalue = parseFloat(0.00);
                    for([code,value] of new_items){ //loop through the transaction
                        if(code == tmpCode){        //check if the transactions made matches the code on the summary header
                           value.forEach(v => { //loop
                                xvalue += parseFloat(v.amount); // push the value
                            })
                        }
                    }
                    payload_of[i].push(xvalue);
                }else{
                    payload_of[i].push(0.00);
                }

            })
         })


        //creation of payload for ba fees
        summaryHeaders[4].map((header,i) => {  //summaryHeaders for other fees. this gets all other fees transactions on the selected date
        let tmpCode = header.split(" - ")[0];   //get the code of each transactions
        payload_ba[i].push(header);             //include the entire header in the payload variable
        
        summary.map((items,index) => {                //loop through the summary array (per user)
            if(!_.isEmpty(items)){            //if user have transactions
                var new_items = Object.entries(items);
                var xvalue = parseFloat(0.00);
                for([code,value] of new_items){ //loop through the transaction
                    if(code == tmpCode){        //check if the transactions made matches the code on the summary header
                        value.forEach(v => {
                            xvalue += parseFloat(v.amount); // push the value
                        })
                    }
                }
                payload_ba[i].push(xvalue);
            }else{
                payload_ba[i].push(0.00);
            }

        })
        })
         _.remove(payload_of,n => { return n.length == 0; })
         _.remove(payload_af,n => { return n.length == 0; })
         _.remove(payload_bf,n => { return n.length == 0; })
         _.remove(payload,n => { return n.length == 0; })
         _.remove(payload_ba,n => { return n.length == 0; })

         $scope.payloadSummary = [payload,payload_af,payload_bf,payload_of,payload_ba];

         $scope.totalSummary = parseFloat(0.00);
         $scope.xHeader = [[],[],[],[],[]];
        

         //total summary,yheaders,xHeader values computation  (located on the last column)
         $scope.payloadSummary.forEach((items,i) =>{
             items.forEach((item,ii) =>{
                var yHeaderComputation = parseFloat(0.00); // y header computations

                 item.forEach((value,index) =>{
                    var xHeaderComputation = parseFloat(0.00);
                     if(index != 0){
                      $scope.totalSummary += value;
                      yHeaderComputation += value;
                      xHeaderComputation += value;
                      $scope.xHeader[i].push(xHeaderComputation); 
                    }
                 })
                $scope.payloadSummary[i][ii].push(yHeaderComputation); //append computed y header to index 11 
             })
         })
         
         //manipulate xheader chunk by 10
         $scope.xHeader.forEach((header,i) => {
            var new_arr = _.chunk(header, $scope.allPaycoUsersLength); //chunk by number of payco users
            $scope.xHeader[i] = new_arr;
         })
         //computed xheader
        $scope.xHeader.forEach((header,index) => {
            $scope.xHeader[index] = _.unzip( $scope.xHeader[index]);
            var totalx = parseFloat(0.00);
           
                $scope.xHeader[index].forEach((items,i) => {
                    var x = parseFloat(0.00);
                    items.forEach(value => {
                        x += value;
                        totalx += value;
                    })
                    $scope.xHeader[index][i] = x;
                
                })
                $scope.xHeader[index].push(totalx);
        });

        //computing total 
        var newX = _.unzipWith($scope.xHeader);
        newX.forEach((header,i) => {
            newX[i] = _.sum(header);
        })
        $scope.totalXheader = newX;        
     })       
}


//===========================apprentice fees report controller==========================

$http.get('./api/_getAllBatchesApprenticeFees.php')
.then(response => {
    $scope.batchApprenticeFees = response.data; 
});  


$http.get('./api/_getAF.php')
.then(response => {
    $scope.af = response.data;
});  



$scope.getBatchesAFReports = function(){
    $scope.feeID = $('#feeIDSelect').val();
    var from = moment($scope.dateRange.split(" - ")[0]).format("M/D/YYYY");
    var to = moment($scope.dateRange.split(" - ")[1]).add(1,'d').format("M/D/YYYY");
    var dateRange = from + " , " + to;
    
    $http.get('./api/_getBatchesAF.php?feeID=' + $scope.feeID + '&dateRange=' + dateRange)
         .then(response => {
            $scope.batchesAFReports = response.data;
         });
         
    

}

$scope.getStudentPerBatch = function(id){
    $http.get('./api/_getAFByBatch.php?id=' + id)
         .then(response => {
            $('#student_batch').addClass('launch');
            $scope.student_batch = response.data;
            $scope.student_batch_courses = _.uniqBy(response.data, 'code');
            // console.log($scope.student_batch_courses);
            
         });

}

$scope.addTR = function(){
    $('#batchTable tr:last').after($compile(`
    
    <tr>
            <td style="background: #343a40;"></td>
            <td>
                <input class="e-control" type="text"  placeholder="@batch description" required>
            </td>
            <td>
                <div class="e-form-group unified">
                    <input class="e-control" type="date"   placeholder="From" required>

                    <input class="e-control" type="date"   placeholder="To" required>
                </div>
            </td>
            <td>
                <input class="e-control" type="text"   placeholder="quota" required>
            </td>
            <td>
            
                <select class="e-select e-control"  placeholder=""  required>
                <option value="">Select Apprentice fee</option>
                
                <option value="{{af.id}}" 
                ng-repeat="af in af">{{af.description}}</option>
                </select>     
                
                
                <select class="e-select e-control mt-3" placeholder="Select Setting" ng-init="getCurrentSettings()" required>
                <option value="" class="text-white" hidden>Select current setting</option>
                <option value="" class="text-white" style="background-color: #5e72e4;">Current
                settings</option>
                <option value="{{setting.id}}" ng-repeat="setting in currentSettings">
                {{setting.description}}</option>
                </select>
        
            </td>
            <td style="vertical-align: middle;">
                    <label for="" class="d-flex centered"><input type="checkbox" name="" id=""></label>
            </td>

            <td>
                <div class="e-form-group unified">
                    <button class="inverted e-btn dark" ng-click="saveBatch()" ><i class="far fa-save"></i> save</button>
                    <button class="inverted e-btn dark" onclick="dismissTR()"><i class="fas fa-times" disabled></i> discard</button>
                </div>
            </td>
        </tr>
    
    `)($scope));
}

$scope.saveBatch = function(){
    $scope.batch = {};
    var tr = event.target.parentNode.parentNode.parentNode;
    $scope.batch.details = $(tr.children[1].children[0]).val();
    $scope.batch.quota = $(tr.children[3].children[0]).val();
    $scope.batch.status = ($(tr.children[5].children[0].children[0]).is(":checked")) ? 1 : 0;
    $scope.batch.from =  moment($(tr.children[2].children[0].children[0]).val()).format("M/D/YYYY"); ;
    $scope.batch.to =  moment($(tr.children[2].children[0].children[1]).val()).format("M/D/YYYY"); ;
    $scope.batch.feeID = $(tr.children[4].children[0]).val();
    $scope.batch.settingID = $(tr.children[4].children[1]).val();

    if( $scope.batch.details == '' ||  $scope.batch.quota == '' ||  $scope.batch.from == 'Invalid date' || $scope.batch.to == 'Invalid date' ||
    $scope.batch.feeID == '' || $scope.batch.settingID == '' || $scope.batch.details.length < 3){
        swal('Form incomplete','enter all fields with correct data','warning')

    }else{
        $http.post('./api/_insertBatch.php', $scope.batch)
        .then(response => {
            swal(response.data.msg,response.data.p,response.data.type);   
            $scope.getBatchesAFReports();
        })
        
    }
}


//===========================graduation fees report controller==========================

$scope.getGraduationFeeReports = function(){

    var course_code = $('#courseSelect').val();
    var from = moment($scope.dateRange.split(" - ")[0]).format("YYYY-M-D");
    var to = moment($scope.dateRange.split(" - ")[1]).add(1,'d').format("YYYY-M-D");
    var dateRange = from + " , " + to;
    
    $http.get('./api/_getGraduationFee.php?course_code=' + course_code + '&dateRange=' + dateRange)
         .then(response => {
             $scope.gradfee_students = response.data;
         })
}

});

