<div ></div>
<?php

include "views/student_tabs.php";


?>

<div class="e-container py-3" ng-init="getEnrollmentHistory(<?php echo $_GET['student'];?>)">


<div class="e-col-12" ng-repeat="e in ehistory">
 <center><h4>{{e.description}}</h4></center>

 <table class="e-table">
    <thead>
        <tr>
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Prelim</th>
                        <th>Midterm</th>
                        <th>Semi Final</th>
                        <th>Final</th>
                        <th>Grade</th>
                        <th style="text-decoration:regular;">Status</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="s in e.subjects">
            <td>{{s.code}}</td>
            <td>{{s.name}}</td>
            <td>{{s.term1}}</td>
            <td>{{s.term2}}</td>
            <td>{{s.term3}}</td>
            <td>{{s.term4}}</td>
            <td>{{s.final}}</td>
            <td>
            <select class="e-control" name="" id="" ng-model="s.remark" disabled>
                <option ng-repeat="st in status" ng-value="{{st.id}}">
                    {{st.description}}
                </option>
            </select>
            </td>
            <td>
                <button class="e-btn primary small" ng-click="convertGrade(s.id)">Convert</button>
            </td>
            <td>
                <button class="e-btn danger small" ng-click="deleteValid(s.id)"><i class="fa fa-trash"></i></button>
            </td>

        </tr>
    </tbody>
 </table>
</div>


</div>


<div id="template" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Convert Grades</p>
      <p>
      PLEASE REVIEW THE DATA CAREFULLY BEFORE CONVERTING 
      </p>
      <button type="button" ng-click="cancel()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    
    <div class="e-modal-body">
    <!--Content-->
    <form method="POST">
        <div class="e-cols">
               <table class="e-table">
               
               <tr>
                <td>Before</td><td>After</td>
               </tr>

               <tr ng-repeat="g in grade">
               <td>{{g.b}}</td><td><input type="text" class="e-control" ng-model="grade.after[$index]"></td>
               </tr>
               
               </table>

               

           
              
               

        </div>         
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
    <center>
      <button class="e-btn inverted" ng-click="cancel()">Cancel</button>
      <button class="e-btn primary" ng-click="saveConverted()">Convert</button>
      </center>
    </footer>
  </div>
</div>


<div id="modals" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">

    </header>
    
    <div class="e-modal-body">
    <center>
    Are you sure?
    <!--Content-->
   
    <br>
    <button class="e-btn rounded primary" ng-click="deleteSubject()">Delete</button>  <button class="e-btn rounded danger" ng-click="cancelValid()">Cancel</button>
    </center>
    </div>
    
  </div>
</div>

<?php
include "update-modal.php";
?>