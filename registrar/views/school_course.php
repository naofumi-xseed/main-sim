

<div  style="text-transform:uppercase">



<div class="e-cols px-1 py-1">
  <div class="e-col" ng-init="fetchSchool()">
  <button class="e-btn btn primary rounded small mt-3" ng-click="insertSchool('Add School')"> <i class="fa fa-plus"></i> SCHOOL</button>
  <input type="text" class="e-control rounded small mt-3" placeholder="Search" ng-model="s" style="width:75%">
    <ul class="e-list ">
    <li class="e-list-item "  ng-repeat="school in schools | filter : s">{{school.code}} - {{school.name}} <a class="align-end" ng-click="updateSchool('Edit School',school.id)"><i class="fa fa-pen"></i></a></li>
    </ul>
  </div>

  <div class="e-col" ng-init="fetchCourse()">  
  <button class="e-btn btn primary rounded small mt-3" ng-click="insertSchool('Add Course')"><i class="fa fa-plus"></i> COURSE</button>
  <input type="text" class="e-control small rounded mt-3" placeholder="Search" ng-model="c"  style="width:78%">
    <ul class="e-list">
  
    <li class="e-list-item"  ng-repeat="course in courses | filter : c">{{course.code}} - {{course.name}} <a class="align-end" ng-click="updateSchool('Edit Course',course.id)"><i class="fa fa-pen"></i></a></li>
    </ul>
  </div>



   
</div>


</div>



<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">{{title}}</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
   <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
              <input type="text"  ng-model="name" placeholder="school" class="e-control">
        </div>
    <!--Content-->  
    </div>
    <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
              <input type="text"  ng-model="code" placeholder="code" class="e-control">
        </div>
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>
      <button ng-if="title==='Add School'" class="e-btn danger" ng-click="insertSchoolnow()">Save School</button>

      <button ng-if="title==='Add Course'" class="e-btn danger" ng-click="insertSchoolnow()">Save Room</button>

      <button ng-if="title==='Edit School'" class="e-btn danger" ng-click="editSchool()">Update School</button>

      <button ng-if="title==='Edit Course'" class="e-btn danger" ng-click="editSchool()">Update Course</button>
    </footer>
  </div>
</div>


