<form  method="POST" id="teacherSearch">
  <input class="searchBar" type="text" name="find">
</form>

<table class="table table-striped">
  <thead>
    <th>Employee No</th>
    <th>Employee Name</th>
    <th>Actions</th>

  </thead>
<tbody id="list">
                  
<?php
$teachers = $db->query('SELECT id,employee_no,first_name,middle_name,last_name,active from teachers where 1 order by id DESC');

                  while($result=$teachers->fetch_assoc()){
//@$counter=$counter+1;
                        if($result['active']==1){
                            $acts ="btn-primary";
                            $status ="Deactivate";
                            $do="deactivate";
                        }
                        else{
                            $acts ="btn-danger";
                            $status = "Activate";
                            $do="activate";
                        }

                      echo"<tr>
                             <td><b>{$result['employee_no']}</b></td> 
                             <td>{$result['last_name']}, {$result['first_name']} {$result['middle_name']}</td>
                             <td>
                              <a href='?test&reset={$result['id']}' class='btn btn-xs btn-warning' style='color' >Reset</a>
                              <a href='?test&edit={$result['id']}' class='btn btn-xs btn-success' ><i class='fa fa-pencil'></i></a>
                              <a href='?test&$do={$result['id']}' class='btn btn-xs $acts' style='color' >$status</a>
                             </td>
                           </tr>
                          ";






                       
      }
 
?>


</tbody>

</table>

</div>

<?php


if(isset($_GET['edit'])){

$mode = "Edit";
       

          //  $edit = "SELECT id,code,name from schools where id='{$_GET['edit']}'";
            
            $edit= "SELECT id,employee_no,first_name,middle_name,last_name,suffix from teachers where id='{$_GET['edit']}'";
        
  

        $con_edit=mysqli_query($db,$edit);

        $res_mod = mysqli_fetch_array($con_edit);






}
else {
    $mode = "Add";
}


?>


<div class="modal fade" id="addschool" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{mode}} Teacher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
        <div class="form-group" ng-model="mode='<?php echo $mode; ?>'">
        
            <form action="" method="POST">
        
            <div class="container" ng-if="mode==='Edit'">
            
           
                <div class="row">
                    <div class="col-sm-1">
                    Employee NO.
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php  echo @$res_mod['employee_no'];?>" name="emp_no" required>
                    </div>
                </div><br>

                

                <div class="row">
                <div class="col-sm-1">
                    First Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php  echo @$res_mod['first_name'];?>" name="f_name" required>
                    </div>
                </div> <br>

                <div class="row">
                <div class="col-sm-1">
                   Middle Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php  echo @$res_mod['middle_name'];?>" name="m_name" required>
                    </div>
                </div> <br>


                <div class="row">
                <div class="col-sm-1">
                   Last Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php  echo @$res_mod['last_name'];?>" name="l_name" required>
                    </div>
                </div> <br>

                <div class="row">
                <div class="col-sm-1">
                    Suffix
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="<?php  echo @$res_mod['suffix'];?>" name="suffix" required>
                    </div>
                </div> 



            </div>

            



            <div class="container" ng-if="mode==='Add'">

            <div class="row">
                    <div class="col-sm-1">
                    Employee NO.
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="" name="emp_no" required>
                    </div>
                </div><br>

                

                <div class="row">
                <div class="col-sm-1">
                    First Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="" name="f_name" required>
                    </div>
                </div> <br>

                <div class="row">
                <div class="col-sm-1">
                   Middle Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="" name="m_name" required>
                    </div>
                </div> <br>


                <div class="row">
                <div class="col-sm-1">
                   Last Name
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="" name="l_name" required>
                    </div>
                </div> <br>

                <div class="row">
                <div class="col-sm-1">
                    Suffix
                    </div>
                    <div class="col-sm-4">
                    <input type="text" class="form-control" value="" name="suffix" required>
                    </div>
                </div> 

           
                
            </div>
         
        <center style='margin-top:7px;'> 
            
 <?php


if(isset($_POST['save']) && isset($_GET['add_teacher'])){

 $hash =  md5($_POST['emp_no']);

@$add_s="INSERT INTO teachers (
     employee_no,
     first_name, 
      middle_name,
      last_name, 
      removed, 
      complete_address, 
      region_id,
       province_id, 
       city_id, 
       barangay_id, 
       street, 
       suffix,
       created_at,
       updated_at,
       password, 
        active
        ) 
VALUES
 (
     '{$_POST['emp_no']}',
     '{$_POST['f_name']}',
     '{$_POST['m_name']}',
     '{$_POST['l_name']}',
     '',
     '',
     '0',
     '0',
     '0',
     '0',
     '',
     '{$_POST['suffix']}',
     '$time',
     '$time',
     '$hash',
     '1'
     
     )";


mysqli_query($db,$add_s);


echo "<p style='color:green'>Successfully added</p>";



}

else if(isset($_POST['save']) && isset($_GET['add_course'])){

    @$add_c="INSERT INTO courses (code,name,created_at) VALUES ('{$_POST['code']}','{$_POST['nama']}','$time')";
    mysqli_query($db,$add_c);

    echo "<p style='color:green'>Successfully added</p>";
    
    
    
    }

else if(isset($_POST['save']) && isset($_GET['edit'])){

    @$edit_s="UPDATE teachers SET employee_no='{$_POST['emp_no']}', first_name='{$_POST['f_name']}', middle_name='{$_POST['m_name']}', last_name='{$_POST['l_name']}', suffix='{$_POST['suffix']}', updated_at='$time' where id='{$_GET['edit']}'";
    mysqli_query($db,$edit_s);

    echo"
    <script>
    window.location='?test&edit={$_GET['edit']}';
    </script>
    ";

    echo "<p style='color:green'>Successfully Updated!</p>";
        
        
        
        }

else if(isset($_GET['deactivate'])){

    @$edit_c="UPDATE teachers SET active='0' where id='{$_GET['deactivate']}'";
    mysqli_query($db,$edit_c);

    echo"
    <script>
    window.location='?test';
    </script>
    ";
    
    echo "<p style='color:green'>Successfully Deactivated!</p>";
        
        
        
        }


        else if(isset($_GET['activate'])){

            @$edit_c="UPDATE teachers SET active='1' where id='{$_GET['activate']}'";
            mysqli_query($db,$edit_c);
        
            echo"
            <script>
            window.location='?test';
            </script>
            ";
            
            echo "<p style='color:green'>Successfully Deactivated!</p>";
                
                
                
                }

                else if(isset($_GET['reset'])){

                    $emp_n = "SELECT id,employee_no from teachers where id='{$_GET['reset']}'";
                    $con_emp =mysqli_query($db,$emp_n);
                    $res_em=mysqli_fetch_array($con_emp);

                    $rehash=md5($res_em['employee_no']);

                    @$edit_c="UPDATE teachers SET password='$rehash' where id='{$_GET['reset']}'";
                    mysqli_query($db,$edit_c);
                
                    echo"
                    <script>
                    window.location='?test';
                    </script>
                    ";
                    
                    echo "<p style='color:green'>Successfully Deactivated!</p>";
                        
                        
                        
                        }



?>
</center>  
  
        </div>
      <div class="modal-footer">
        <a href="?test" type="button" class="btn btn-secondary pull-left" style="background-color:gray;color:white;">Close</a>
      
        <button type='submit' id="rel" class="btn btn-success" value='Login' name="save"><i class="fa fa-check"></i> save</button></form>
      </div>
    </div>
  </div>
</div>


<?php


if(isset($_GET['add_teacher']) || isset($_GET['add_course']) || isset($_GET['edit']) || isset($_GET['edit_school'])){
       echo"<script type='text/javascript'>
 


$('#addschool').modal('toggle');

 
              
            
</script>";


}


?>


  <script>

     
      $('.searchBar').keyup(function(){
            // console.log(this.value);
            $.ajax({
                type: "POST",
                url: './api/searchTeacher.php',
                data: $("#teacherSearch").serialize(),
                success: function (data) {
                    $('#list').html(data);
                }
            });
  
      });
   
        
    </script>