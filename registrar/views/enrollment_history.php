<div ></div>
<?php

include "views/student_tabs.php";


?>

<div class="e-container py-3" ng-init="getEnrollmentHistory(<?php echo $_GET['student'];?>)">


<div class="e-col-12" ng-repeat="e in ehistory">
 <center><h4>{{e.description}}</h4></center>

 <table class="e-table">
    <thead>
        <tr>
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Prelim</th>
                        <th>Midterm</th>
                        <th>Semi Final</th>
                        <th>Final</th>
                        <th>Grade</th>
                        <th>Remarks</th>
                        <th>Teacher</th>
                        <th style="text-decoration:regular;">Status</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="s in e.subjects">
            <td>{{s.code}}</td>
            <td>{{s.name}}</td>
            <td>{{s.term1}}</td>
            <td>{{s.term2}}</td>
            <td>{{s.term3}}</td>
            <td>{{s.term4}}</td>
            <td>{{s.final}}</td>

        </tr>
    </tbody>
 </table>
</div>


</div>