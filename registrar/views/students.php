<div class="e-cols-12">
    <div class="e-col-12 pt-1 pl-2">
    <input id='search' class="e-control" type='text'  placeholder="Search" ng-model='search' style='background-color:white; width:200px;'>
    </div>
</div>

<div class="e-cols-12">
<div class="e-col-12  pb-5 pt-1 pl-2">
    <dir-pagination-controls max-size="10" direction-links="true" boundary-links="true" >
    </dir-pagination-controls>

    </div>

</div>





<div class="e-cols-12">
<div class="e-col-12  pb-2 pt-4 pl-2">
<table class="e-table table-striped" style="text-transform: uppercase;" ">
    <thead>
    <tr>
    <th>Student ID</th>
    <th>Name</th>
    <th>Strand</th>
    <th>YEAR</th>
    <th>Status</th>
    </tr>
    </thead>

    <tbody ng-init="studentList(0)">
        <tr dir-paginate="student in students | filter : search  | itemsPerPage:10 ">
            <td><a href="?student_info={{student.student_id}}">{{student.student_no}}</td>
            <td>{{student.last_name}}, {{student.first_name}} {{student.last_name}}</td>
            <td>{{student.code}}</td>
            <td>{{student.short_name}}</td>
            <td ng-init="stat=student.status=='Enrolled' ? '#32CD32' : '#F08080'" style="color:{{stat}}"><b>{{stat=='#32CD32' ? 'ACTIVE' : 'INACTIVE'}}</b></td>
            <td><button class="e-btn primary small" ng-click="resetStud(student.student_id,student.student_no)">Reset</button></td>
        </tr>
    </tbody>

</table>

</div>

</div>






