
<?php
include "views/student_tabs.php";
?>
<div class="e-cols">
    <div class="e-col-6">
    <table class="e-table">
        <thead>
            <tr>
                <th>Description</th>
                <th>First Quarter</th>
                <th>Second Quarter</th>
                <th>Average</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="s in subinfo">
                <td>{{s.name}}</td>
                <td>{{s.term1}}</td>
                <td>{{s.term4}}</td>
                <td>{{s.final}}</td>
                <td>{{s.final > 5 && s.final < 75 || s.final === NaN ? 'FAILED' : 'PASSED'}}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td>GPA</td>
                <td>{{getAverage()}}</td>
                <td><b>{{getAverage() > 5 && getAverage() < 75  || s.final === NaN  ? 'FAILED' : 'PASSED'}}</b></td>
            </tr>
        </tfoot>
    </table>
    </div>
    <div class="e-col-6">
    <table class="e-table">
        <thead>
            <tr>
                <th>Core Values</th>
                <th>Description</th>
                <th>First Quarter</th>
                <th>Second Quarter</th>
            
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="v in student_values">

            <td>{{v.desc}}</td>
            <td>{{v.vname}}</td>
            <td>{{v.rname}}</td>
            <td></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
             
                
            </tr>
        </tfoot>
    </table>
    </div>
    
</div>



<!-- //Select Specific Subject -->
<!-- <div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Select Subject</p>
  
      <button type="button" ng-click="cancelSchedule()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body"> -->
    <!--Content-->
   <!-- <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
          <ul class="e-list e-x shadow-6" >
                <li ng-repeat="sc in thesubjects" class="e-list-item">
                      <b class="text-gray" style="text-transform:uppercase">{{sc.subject}} - {{sc.title}}</b>
                      <table>
                      <tr ng-repeat="sec in sc.section">
                      <td>{{sec.section_name}}   
                       <ul class="e-list e-x shadow-6" >
                       <li ng-repeat="sss in sec.sched" class="e-list-item">
                       {{sss.start_time}} - {{sss.end_time}}
                       </li>
                      </ul></td>
                      </tr>
                      </table>                  
                </li> 
          </ul>
        </div>
   </div> -->
    <!--Content-->  
    <!-- </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelSchedule()">Cancel</button>
      <button class="e-btn danger" ng-click="insert('track')">Save changes</button>
    </footer>
  </div>
</div> -->