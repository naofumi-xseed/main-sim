<div class="e-contailer">
<div class="e-cols">
      <div class="e-col mt-5">
          <nav class="e-tabs align-start">
            <ul ng-init="getProg()">
                  <li ng-repeat="prog in progs" ><a href="" ng-click="getSubjectParent(prog.id,'subjects')">{{prog.name}}</a></li>
            </ul>
          </nav>
      </div>
      <div class="e-col mt-4 align-end">
          <input class="e-control rounded" type="text" placeholder="Search..." ng-model="search" style="width:250px">
      </div>
</div>
<div class="e-list">
<div class="e-cols">
  <div class="e-col">
      <a class="e-btn circle inverted purple" ng-click="adding('add')"><i class="fas fa-plus"></i></a> 
  </div>
  <div class="e-col align-end">
  <table class="">
      <tr>
        <td>w - Weeks</td><td>&nbsp&nbsp&nbsp&nbspLh - Lecture Hours</td>
      </tr>
      <tr>
        <td>Th - Total Hours</td><td>&nbsp&nbsp&nbsp&nbspu - Units</td>
      </tr>
  </table>
  </div>
</div>
 <!-- subject dashboard-->
<div class="e-cols e-x shadow-5">
        <div class="e-col">
          <a class="e-list-item" href=""  ng-repeat="subject_p in subject_parents | filter : search" ng-click="getSubjectInfo(subject_p.sub_id,'subject')"><b style="text-transform:uppercase">{{subject_p.code}} - {{subject_p.sp_name}}</b><br>
              <p>
                  <span class="e-tag purple align-end" style="text-transform: uppercase">{{subject_p.st_name}}</span>
                  <span class="e-tag gray rounded">w : {{subject_p.weeks}}</span>
                  <span class="e-tag dark rounded">Lh : {{subject_p.lecture_hours}}</span>
                  <span class="e-tag gray rounded">Th : {{subject_p.lecture_hours * subject_p.weeks }}</span>
                  <span class="e-tag danger rounded">u : {{subject_p.units}}</span>
              </p>
          </a>     
        </div>
        <div class="e-col e-x shadow-5" ng-if="subject_info_id">
        <div class="e-list">
        <div class="marked p-3" >
            <b >Subject Details</b> 
            <a class="e-btn small danger align-end rounded" ng-click="getPrerequisites(subject_info[0].sub_id)">Prerequisite</a>
            <a class="e-btn small success align-end rounded" ng-click="adding('update')">Edit</a>
            <a class="e-btn small purple align-end rounded" ng-click="deleteValid(subject_info[0].sub_id)">Delete</a>
        </div>
        <a class="e-list-item"><b>Name : </b> {{subject_info[0].code}} - {{subject_info[0].sp_name}}</a>
        <a class="e-list-item"><b>Subject Type : </b>{{subject_info[0].st_name}}</a>
        <a class="e-list-item"><b>Weeks : </b>{{subject_info[0].weeks}}</a>
        <a class="e-list-item"><b>Lecture Hours : </b>{{subject_info[0].lecture_hours}}</a>
        <a class="e-list-item"><b>Total Hours : </b>{{subject_info[0].lecture_hours * subject_info[0].weeks}}</a>
        <a class="e-list-item"><b>Units : </b>{{subject_info[0].units}}</a>
      </div>
        </div>
</div>
 <!-- subject dashboard -->

</div>


</div>



<div id="add" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">{{mtitle}}</p>
      <button type="button" ng-click="cancel()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>

       <!--Content add-->
    <div class="e-modal-body" >
 
    <form method="POST">        
        <input type="text" ng-model="scode" class="e-control rounded" placeholder="Subject Code"><br>
        <input type="text" ng-model="sname" class="e-control rounded" placeholder="Subject Name">

        <select name="" class="e-control rounded wt-10" ng-model="subtype" id="" ng-init="getSubjectType()">
        <option value="">Select Subject Type</option>
        <option ng-repeat="stype in stypes" value="{{stype.id}}">{{stype.name}}</option>
        
        </select>
                <input type="text" ng-model="week"    class="e-control rounded">
                <input type="text" ng-model="lecture" class="e-control rounded">
                <input type="text" ng-model="unit"   class="e-control rounded">
    </form>

    </div>


    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancel()">Cancel</button>
      <button class="e-btn danger" ng-click="insertSubject()">Save changes</button>
    </footer>
  </div>
</div>



<div id="prereq" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Prerequisites</p>     
      <button class="e-btn danger small rounded" ng-click="insertPrerequisite()">Add</button>
      
      <select type="text" ng-model="giver" class="e-control rounded" style="width:180px;height:auto">
      <option value="">Select Parent Subject</option>
      <option ng-repeat="subject_p in subject_parents | filter : searchsub" value="{{subject_p.sub_id}}">{{subject_p.code}}</option>
      </select>
      <br>
      <input type="text" ng-model="searchsub" placeholder="Search" class="e-control " style="width:150px;height:21px">
      <button type="button" ng-click="cancelPr()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    
    <div class="e-modal-body">
    <!--Content-->
    <ul>
    <li ng-repeat="prerequisite in subject_prerequisites" style="font-size:11px">
            <b>{{prerequisite.name}}
              <i class="fa fa-times" ng-click="deletePrerequisite(prerequisite.prerequisite_subject_parent_id)" style="cursor:pointer;"></i>
            </b>
        <ol>
            <li ng-repeat="prerequisite_subject in subject_from_prerequisite | filter :  { sub_pid : prerequisite.prerequisite_subject_parent_id}" class="pl-5" style="text-decoration:italic;font-size:10px;">
            - {{prerequisite_subject.sub_name}}
            </li>
        </ol>
    </li>
    </ul>
    </div>
    
  </div>
</div>


<div id="modals" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">

    </header>
    
    <div class="e-modal-body">
    <center>
    Are you sure?
    <!--Content-->
   
    <br>
    <button class="e-btn rounded primary" ng-click="deleteParentSubject()">Delete</button>  <button class="e-btn rounded danger" ng-click="cancelValid()">Cancel</button>
    </center>
    </div>
    
  </div>
</div>