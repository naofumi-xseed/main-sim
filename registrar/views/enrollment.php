<div ></div>
<?php

include "views/student_tabs.php";


?>

<div ng-if="year && sett">
<button  class="e-btn btn success rounded" ng-click="selectSchedules(curriculum.cur_id)" ng-disabled="enrollmentsubs.length>0">Select Section</button>
<button class="e-btn btn green-gradient rounded" ng-disabled="enrollmentsubs.length>0" ng-click="selectionSubs(curriculum.cur_id)">Add Subject</button>
<button class="e-btn btn blue-gradient rounded small" ng-click="openDiscounts()" ng-disabled="!payments">Add Discounts</button>


<button class="e-btn btn blue-gradient rounded small" ng-click="openVoucher()" ng-disabled="!payments">Add Voucher</button>


<select name="" ng-model="disc" id="" ng-if="discounts" class="e-control rounded small" style="width:25%" ng-change="paymentDiscounts(sett,students.strand_id,year,students.student_id,disc)">

<option value="">Select Discount</option>
<option ng-repeat="discount in discounts" value="{{discount.id}}" >
  {{discount.description}} - ( {{discount.fee_amount_type === 2 ? discount.amount*100 : discount.amount }} {{discount.fee_amount_type === 2 ? '%' : '' }})
</option>
</select>


<select name="" ng-model="vdisc" id="" ng-if="vdiscounts" class="e-control rounded small" style="width:25%" ng-change="paymentVoucher(sett,students.strand_id,year,students.student_id,vdisc)">

<option value="">Select Discount</option>
<option ng-repeat="vdiscount in vdiscounts" value="{{vdiscount.fid}}" >
  {{vdiscount.code}} - ( {{vdiscount.voucher_amount}})
</option>
</select>
</div>



<div class="e-cols" >

  <div class="e-col-12" ng-if="subs.length>0">
  <table class="e-table small" >
      <thead>
            <tr>
            <th>Subject Code</th>
            <th>Subject Item</th>
            <th>Hours</th>
            <th>Unit</th>
            <th>Section</th>
            <th>Schedule</th>
            </tr>
      </thead>
      <tbody>
      <tr ng-repeat="sched in subs[0].details">
      <td>{{sched.code}}</td>
      <td>{{sched.name}}</td>
      <td>{{year ==='1' || year ==='2' ? sched.hours : '0'}}</td>
      <td>{{year !=='1'  || year !=='2' ? sched.units : '0'}}</td>
      <td></td>
      <td>
      <table>
        <tr>
            <td>DAY</td><td>TIME</td><td>ROOM</td>
        </tr>
        <tr ng-repeat="detail in sched.schedule_subject">
            <td>{{detail.day}}</td><td>{{detail.start_time}} - {{detail.end_time}}</td><td>{{detail.room}}-{{detail.building}}</td><td>{{detail.teacher}}</td>
        </tr>
      </table>
      </td>

      <td ng-click="removeSub($index)"><button class="e-btn outlined sky">X</button></td>
      </tr>

    

      </tbody>
      

  </table>

  <center><button ng-show="subs.length>0" class="e-btn green-gradient rounded" ng-click="insertSubSched()"  ng-cloak>{{verify === 0 ? 'Assess now' : 'Assessed'}}</button></center>
  </div>

  <div class="e-col-12" ng-if="subs.length===0">
  <table class="e-table small" >
      <thead>
            <tr>
            <th>Subject Code</th>
            <th>Subject Item</th>
            <th>Hours</th>
            <th>Unit</th>
            <th>Section</th>
            <th>Schedule</th>
            </tr>
      </thead>
      <tbody>
      <tr ng-repeat="sched in enrollmentsubs">
      <td>{{sched.code}}</td>
      <td>{{sched.name}}</td>
      <td>{{year ==='1' || year ==='2' ? sched.hours : '0'}}</td>
      <td>{{year !=='1'  || year !=='2' ? sched.units : '0'}}</td>
      <td></td>
      <td>
      <table>
        <tr>
            <td>DAY</td><td>TIME</td><td>ROOM</td>
        </tr>
        <tr ng-repeat="detail in sched.schedule_subject">
            <td>{{detail.day}}</td><td>{{detail.start_time}} - {{detail.end_time}}</td><td>{{detail.room}}-{{detail.building}}</td><td>{{detail.teacher}}</td>
        </tr>
      </table>
      </td>

      <td ng-click="removeEnroll(sched.id)"><button class="e-btn outlined sky">X</button></td>
      </tr>


      </tbody>
      

  </table>

 
  </div>



</div>
{{subs}}


<div id="loader-7"></div>

<!-- {{selectedsub}} -->


<table class="e-table ml-2" style="width:50%" ng-if="payments">
  <thead class="e-thead primary">
        <tr>
          <th>DESCRIPTION</th>
          <th>AMOUNT</th>
        </tr>
  </thead>
  <tbody>
        <tr ng-repeat="tuition in payments">
          
          <th ><a style="color:#7EC0EE">{{tuition.name}}</a>
            
                    <div ng-repeat="fee in tuition.fees" class="e-cols no-gap">
                   
                    
                    <div class="e-col">
                    <hr ng-if="fee.fee_name==='Total'">
                        {{fee.fee_name}}
                      </div>
                    </div>
              
              
          </th>
          <th>{{tuition.tot}}
                    <div ng-repeat="fee in tuition.fees" class="e-cols no-gap">
                    
                       <div class="e-col">
                       <hr ng-if="fee.fee_name==='Total'">
                          {{fee.amount}} 
                      </div>
                      <div class="e-col">
                      <a ng-show="fee.discount"  ng-click="removeDiscount(fee.discount_id)" class="danger small" style="margin-top:0;margin-bottom:0; cursor:pointer; color:red">x</a>
                      <a ng-show="fee.vdiscount"  ng-click="removeVoucher(fee.vdiscount_id)" class="danger small" style="margin-top:0;margin-bottom:0; cursor:pointer; color:red">x</a>
                       </div>
                    
                    
                    </div>
          </th>


               
        </tr>
  </tbody>
</table>

<!-- loader window -->
<div id="load" class="e-modal" >
  <div class="e-modal-content eUp" style="background:transparent">
    <div class="e-modal-body" id="loader-7">
    <center><img  src="../_public/photos/loading.gif" alt="" style="width:100px; height:100px" ng-cloak></center>
    <center style="color:white"><b>Please wait...</b></center>
    </div>
  </div>
</div>
<!-- loader window -->


<div id="schedule" class="e-modal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">SELECT SCHEDULE</p>
  
      <button type="button" ng-click="cancelSchedule()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
   <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
          <ul class="e-list e-x shadow-6" >
                <li ng-repeat="sc in schedules[0]" class="e-list-item">
                      <b class="text-sky" style="text-transform:uppercase">{{sc.name}}</b>
                      <!-- insertSubSched(sc.id) for insert sched -->
                       <button class="e-btn sky rounded small" style="float:right" ng-click="fetchSubSched(sc.id)"> Select</button>
                      <ul class="e-list">
                          <li class="e-list-item" ng-repeat="detail in schedules[1] | filter : { schedule_id : sc.id }">
                          <b>{{detail.code}}</b> - <a style="font-style:italic;font-size:9px">{{detail.name}}</a>
                            <table class="e-table">
                                  <tr  ng-repeat="sd  in schedules[2] | filter : {schedule_detail_id : detail.sched_id}">
                                        <td>{{sd.short_name}}</td>
                                        <td>{{sd.start_time}} - {{sd.end_time}}</td>
                                        <td>{{sd.first_name}} - {{sd.last_name}}</td>
                                  </tr>
                            </table>
                          </li>                          
                      </ul>                      
                </li> 
          </ul>
        </div>
   </div>
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelSchedule()">Cancel</button>
      <button class="e-btn danger" ng-click="insert('track')">Save changes</button>
    </footer>
  </div>
</div>





<!-- //Select Specific Subject -->

<div id="modals" class="e-modal" style="">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Select Subject</p>
      <input type="text" class="e-control rounded" ng-model="search" placeholder="Search">
      <button type="button" ng-click="cancelSchedule()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
   <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
          <ul class="e-list e-x shadow-6" >
                <li ng-repeat="sc in thesubjects | filter : search" class="e-list-item">
                      <b class="text-gray" style="text-transform:uppercase">{{sc.subject}} - {{sc.title}}</b>
                      <table class="e-table">
                      <tbody>
                      <tr ng-repeat="sec in sc.section | filter : search">
                      <td><button class="e-btn primary small" ng-click="fetchPer(sc.sub_id,sec.id)">Select</button></td>
                          <td>{{sec.section_name}} </td>   
                          <td>
                            <table class="e-table">
                            <tr ng-repeat="sss in sec.sched">
                            <td>{{sss.short_name}} - </td>
                            <td>{{sss.start_time}} - {{sss.end_time}}</td>
                            <td>{{sss.first_name}}</td>
                            </tr>
                            </table>
                          </td>
                      
                      </tr>
                      </tbody>
                      </table>
                 
                </li> 
          </ul>
        </div>
   </div>
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelSchedule()">Cancel</button>
      <button class="e-btn danger" ng-click="insert('track')">Save changes</button>
    </footer>
  </div>
</div>