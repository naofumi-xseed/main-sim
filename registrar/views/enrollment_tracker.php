<div ></div>
<?php

include "views/student_tabs.php";


?>

<div class="e-container py-3" ng-init="getEnrollmentTracker(<?php echo $_GET['student'];?>)">


<div class="e-col-12" ng-repeat="e in ehistory">
 <h4>{{e.short_name}}</h4>

 <table class="e-table">
    <thead>
        <tr>            <th>Strand</th>
                        <th>Semester</th>
                        <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="t in e.transactions" >
        
            <td>{{t.strand}}</td>
            <td>{{t.description}}</td>
            <td>{{t.enroll_date}}</td>

        </tr>
        <tfoot>
            <tr>
               
            </tr>
        </tfoot>
    </tbody>
 </table>
 <center><b ng-show="e.transactions.length==0">No transactions</b></center>
</div>


</div>