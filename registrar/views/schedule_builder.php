<div class="e-container">
<form method="POST">
<div class="e-form-group unified align-end" style="margin-top:14px;margin-right:14px;">
  <div class="e-control-helper">
    <i class="far fa-clipboard text-danger"></i>
  </div>
  <select class="e-select" ng-model="setting" ng-change="getSchedules()">
    <option>Select Setting</option>
    <option value="{{set.id}}" ng-repeat="set in settings">{{set.description}}</option>
  </select>
</div>
</form>

<div class="e-card blue-gradient  mb-1 align-start" style="width: 20rem;">
                      
                          <div class="card-body" ng-init="status=curricula.is_active==='0' ? 'Activate' : 'Deactivate'">
                                <h6 class="card-title" style="text-transform:uppercase; font-size:14px;">{{curricula.name}}</h6>
                                    <p class="card-text">{{setdesc}}</p>
                                      <a class="e-btn danger fullwidth" ng-if="status==='Deactivate'">{{status}}</a>
                                      <a class="e-btn success fullwidth" ng-if="status==='Activate'">{{status}}</a>
                          </div>
</div>
<div  ng-init="department()">
                <nav class="e-tabs">
                  <ul>                    
                        <li ng-repeat="dept in departments" style="font-size:12px" ng-model="atab=dep_id===dept.id ? 'active' : 'n'" class="{{atab}}"><a href=""  ng-click="initDepartment(dept.id)">{{dept.code}}</a></li>
                  </ul>
               </nav>
</div>
                      
                        <table class="e-table" style="text-transform:uppercase;">
                                <thead class="rounded blue-gradient">
                                        <tr>
                                        <th>SECTION</th>
                                        <th>COURSE</th>
                                        <th>YEAR</th>
                                        <th>ADVISER</th>
                                        <th>AVAILABLE</th>
                                        </tr>
                                </thead>
                                <tbody ng-init="getSchedules()">
                                        <tr ng-repeat="sc in schedules">
                                                <td><a class="urls" href="?builder_sched={{sc.sched_id}}&setting={{sc.setting_id}}">{{sc.sec_name}}</a></td>
                                                <td>{{sc.str_name}}</td>
                                                <td>{{sc.short_name}}</td>
                                                <td>{{sc.last_name}}, {{sc.first_name}} {{sc.middle_name}}</td>
                                                <td>
                                                  <button class="e-btn small primary" ng-click="editSched(sc.sched_id,sc.sec_name,sc.str_id,sc.yl_id,sc.t_id)">edit</button>
                                                  <button class="e-btn small danger">delete</button>
                                                </td>     
                                        </tr>

                                </tbody>
                        </table>
                        
</div>


<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Edit Schedule</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
    <div class="e-cols">
                 
                    <div class="e-col-3">
                    NAME
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="emp_no" ng-model="sname" required>
                    </div>
                </div><br>

                

                <div class="e-cols">
                <div class="e-col-3">
                    STRAND
                    </div>
                    <div class="e-col-9">
                    <select name="" class="e-control" ng-model="str" id="">
                    <option ng-repeat="strand in strands" ng-value="{{strand.id}}">{{strand.name}}</option>
                    </select>
                    </div>
                </div> <br>

                <div class="e-cols">
                <div class="e-col-3">
                   YEAR LEVEL
                    </div>
                    <div class="e-col-9">
                    <select name="" class="e-control" ng-model="ylevel" id="">
                    <option ng-repeat="year in years" ng-value="{{year.id}}">{{year.in_word}}</option>
                    </select>
                    </div>
                </div> <br>


                <div class="e-cols">
                <div class="e-col-3">
                   TEACHER
                    </div>
                    <div class="e-col-9">
                    <select name="" class="e-control" ng-model="teacher_id" id="">
                    <option ng-repeat="teacher in teachers" ng-value="{{teacher.id}}">{{teacher.last_name}}, {{teacher.first_name}}</option>
                    </select>
                    </div>
                </div> <br>

                <!-- <div class="e-cols">
                <div class="e-col-3">
                    Suffix
                    </div>
                    <div class="e-col-9">
                    <input type="text" class="e-control" name="suffix" ng-model="suffix" required>
                    </div>
                </div>  -->
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>


      <button type='submit' id="rel" class="e-btn success" ng-click="updateSched()"><i class="fa fa-check"></i> save</button>

    </footer>
  </div>
</div>
