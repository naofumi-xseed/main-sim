<div class="px-3" ng-init="getBlocking()">

  <input type="text" class="e-control rounded" style="width:30%" placeholder="Search" ng-model="search"><br><br>
  <table class="e-table">
      <thead>
        <tr>
            <th>Student Number</th>
            <th>Name</th>
            <th>Remark</th>
            <th>Action</th>
        </tr>
      </thead>  
      <tbody>
        <tr ng-repeat="b in blockings | filter : search">
            <td>{{b.student_number}}</td>
            <td>{{b.full_name}}</td>
            <td>{{b.blocking.remarks}}</td>
            <td>
            <button class="e-btn small {{b.blocking.is_active === 1 ? 'success' : 'danger'}}" ng-click="block(b.id,b.blocking.is_active)">{{b.blocking.is_active === 1 ? 'Unblock' : 'Block'}}</button>
            </td>
        </tr>
      </tbody>
  </table>
</div>


<div id="modals" class="e-modal" >
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Student Block</p>
  
      <button type="button" ng-click="cancelModal()" class="e-delete">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
    <!--Content-->
   <div class="e-cols">
        <div class="e-col-12" style="font-size:12px">
              <input type="text"  ng-model="remark" placeholder="Reason" class="e-control">
        </div>
    <!--Content-->  
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn inverted" ng-click="cancelModal()">Cancel</button>
      <button  class="e-btn danger" ng-click="newblock()">Save changes</button>
    </footer>
  </div>
</div>


