<?php
require "pdo_db.php";


class Payments{

    private $con;
    public  $lab;
    public  $unit;
    public  $lechour;
    public  $enrollment;
    public  $tuition;
    public  $summary;
    public  $misc;
    public  $datenow;
    public  $payload;
    public  $echeck;

    public function __construct($db){
        $this->con=$db;

    }
    public function enrollment(){

        $stmt = $this->con->prepare("SELECT id,setting_id,fees_summary FROM enrollment_student WHERE setting_id ='{$_GET['s']}' && student_id='{$_GET['student']}'");
        $stmt->execute();

        //enrollment datas
        $this->enrollment   = $stmt->fetch();
        $this->summary = json_decode($this->enrollment['fees_summary']);

        //date of transaction
        $date    = new DateTime('now', new DateTimeZone('Asia/Manila'));
        $this->datenow = $date->format('Y-m-d H:i:s');

        //enrollment checker
        $this->echeck =   $stmt->rowCount();

    
    }

    
    public function select(){  
        //check if there is enrollment
        if($this->echeck>0){
         
         $payment_types = array();
         
         $back = $this->con->prepare("SELECT * FROM student_back_accounts WHERE enrollment_student_id='{$this->enrollment['id']}'");
         $back->execute();

         $ba = $back->fetch()['amount'];

   


         $p_type = $this->con->prepare("SELECT * FROM fee_type");
         $p_type->execute();

         while($pt_data = $p_type->fetch()){

         
         
           $pt_index = count($payment_types);
           $payment_types[$pt_index] = array(
                                                'pt_id' => $pt_data['id'],
                                                'name'  => $pt_data['name'],
                                                'fees'  => array(),
                                                'tot'   => array()
                                            );
           //back account 
           if($pt_data['name'] === 'BACK ACCOUNT'){ $payment_types[$pt_index]['tot'] =  empty($ba) ? '0' : $ba;}

            $p_amounts = $this->con->prepare("SELECT f.id as f_id ,f.name as f_name,f.fee_type_id as ft_id,
                                                            fs.*
                                                            FROM 
                                                            fee_structure as fs 
                                                            INNER JOIN fees as f ON f.id = fs.fee_id
                                                            WHERE f.fee_type_id='{$pt_data['id']}' && fs.strand_id='{$_GET['st']}' && fs.year_level_id ='{$_GET['y']}' && fs.setting_id='{$_GET['s']}'");
            $p_amounts->execute(); 
         
                       while($fees = $p_amounts->fetch()){
                        
                        if($fees['f_name']== 'tuition fee'){
         
                                 $this->tuition = $fees['fee_amount'];
                                 @$payment_types[$pt_index]['tot'] =  number_format($this->summary[0]->tuition_fee);
                        }
                        
                        else if($pt_data['name'] == 'MISCELLANEOUS FEE'){
         
                                 $this->misc = $this->misc+$fees['fee_amount'];
                                 @$payment_types[$pt_index]['tot'] =  number_format($this->summary[0]->misc_fee,2);
                        }
                       
                    
                        else{
                           $Total ='';
                        }
                        
                        
                          
                          $payment_types[$pt_index]['fees'][] = array(
                                                                        'fee_name' => $fees['f_name'],
                                                                        'amount'   => $fees['fee_amount'],
                                                                        'total'    => 1
                                                                      );                                                                      
                      }
                                                 
         }

      
            @$payment_types[$pt_index]['fees'][] = array(
                'fee_name' => 'Laboratory',
                'amount'   => number_format($this->summary[0]->lab_fee,2),
                'total'    => ''
            );
            //discount name
            $dn = $this->con->prepare("SELECT description FROM discounts WHERE id = :id");

             
            foreach($this->summary[0]->discount as $di){

                $dn->execute([':id' => $di->id]);

                @$payment_types[$pt_index]['fees'][] = array(
                    'fee_name'    => $dn->fetch()['description'],
                    'amount'      => $di->amount,
                    'total'       => '',
                    'discount_id' => $di->id,
                    'discount'    => 1
                );

            }
             //voucher  name
             $vn = $this->con->prepare("SELECT description FROM vouchers WHERE id = :id");

            foreach($this->summary[0]->voucher_discount as $vi){
                
                $vn->execute([':id' => $vi->id]);

                @$payment_types[$pt_index]['fees'][] = array(
                    'fee_name'    => $vn->fetch()['description'],
                    'amount'      => $vi->amount,
                    'total'       => '',
                    'vdiscount_id' => $vi->id,
                    'vdiscount'    => 1
                );

            }
            @$payment_types[$pt_index]['fees'][] = array(
                'fee_name' => 'Subtotal',
                'amount'   => number_format($this->summary[0]->total_tuition_fee,2),
                'total'    => ''
            );
            @$payment_types[$pt_index]['fees'][] = array(
                'fee_name' => 'Total',
                'amount'   => number_format($ba+$this->summary[0]->total_tuition_fee,2),
                'total'    => ''
            );
           
     
         //other objects
            
         
         echo json_encode($payment_types,JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;
         
        }//if there is enrollment

        else{
            
        }
    }  

    public function create_payment()  
    {  
         
            //tuitions requirements
            //for college
            $sqlUnits       = $this->con->prepare("SELECT SUM(s.units) as sum_units FROM `student_subjects` as ss
            LEFT OUTER JOIN subjects as s
            ON ss.subject_id = s.id 
            WHERE ss.setting_id='{$_GET['s']}' AND ss.student_id='{$_GET['student']}'
            ");
            $sqlUnits->execute();

            //for shs
            $sqlLecHours    = $this->con->prepare("SELECT SUM(s.lecture_hours * s.weeks) as sum_hours FROM `student_subjects` as ss
            LEFT OUTER JOIN subjects as s
            ON ss.subject_id = s.id 
            WHERE ss.setting_id='{$_GET['s']}' AND ss.student_id='{$_GET['student']}'
            ");
            $sqlLecHours->execute();

            $sqlLABTotal    = $this->con->prepare("SELECT DISTINCT 
            f.subject_amount as sumlab
            FROM student_subjects as ss
            LEFT OUTER JOIN fee_subjects as f 
            ON ss.subject_id = f.subject_id
            WHERE f.strand_id = '{$_GET['st']}' AND f.setting_id = '{$_GET['s']}'
            ");
            $sqlLABTotal->execute();


            // operations
            $pluser= 0;while($lab = $sqlLABTotal->fetch()['sumlab']){$totlab =  $pluser + $lab;}

            if($_GET['y'] == 1 || $_GET['y'] == 2){
            $TotalLecHours  = 0;
            $TotalLecHours  = $sqlLecHours->fetch()['sum_hours'];
            $TotalTuition   = $TotalLecHours;
            }else{
            $TotalUnits     = 0;
            $TotalUnits     = $sqlUnits->fetch()['sum_units'];
            $TotalTuition   = $TotalUnits;
            }


            $tfee        = $this->tuition *$TotalTuition;
            $sumtotal    = $tfee+$this->misc+$totlab;
            $this->payload[]   = array(       
                                        'total_tuition_fee'          => $sumtotal,
                                        'tuition_fee'                => $tfee,
                                        'misc_fee'                   => $this->misc,
                                        'lab_fee'                    => $totlab,
                                        'discount'                   => empty($this->summary[0]->discount) ? [] : $this->summary[0]->discount, //remove if there is error 
                                        'voucher_discount'           => empty($this->summary[0]->voucher_discount) ? array() : $this->summary[0]->voucher_discount,
                                        'tuition_summary'            => [[$this->tuition],[0],[0]],
                                        'tuition_payment_timestamps' => [$this->datenow]
                                );
    }
    public function add_discount()
    {

        $ds = $this->con->query("SELECT * from discounts where id='{$_GET['discount']}'");
        $res_ds = $ds->fetch();
  
        if($res_ds['fee_amount_type'] == 2){
          $discount = $this->summary[0]->total_tuition_fee*$res_ds['amount'];
        }
        else{
          $discount = $res_ds['amount'];
        }
     


        $dsc = array(
              'id'      => $_GET['discount'],
              'amount'  => $discount
        );
       
        array_push($this->summary[0]->discount, $dsc);
      
        $this->payload[]                    = array(
                                                    'total_tuition_fee'          =>  $this->summary[0]->total_tuition_fee - $discount,
                                                    'tuition_fee'                =>  $this->summary[0]->tuition_fee,
                                                    'misc_fee'                   =>  $this->summary[0]->misc_fee,
                                                    'lab_fee'                    =>  $this->summary[0]->lab_fee,
                                                    'discount'                   =>  $this->summary[0]->discount,
                                                    'voucher_discount'           =>  $this->summary[0]->voucher_discount,
                                                    'tuition_summary'            =>  $this->summary[0]->tuition_summary,
                                                    'tuition_payment_timestamps' =>  $this->summary[0]->tuition_payment_timestamps
                                                  );
      
        //insert discount
        $idis=$this->con->prepare("INSERT INTO student_discounts (enrollment_student_id,discount_id) VALUES ('{$this->enrollment['id']}','{$_GET['discount']}')");
        $idis->execute();
                                 

    }
    public function add_voucher()
    {

       $checkv = count($this->summary[0]->voucher_discount);

       if($checkv>0){

        $dsc[] = array(
            'id'        => $this->summary[0]->voucher_discount[0]->id,
            'amount'    => $this->summary[0]->voucher_discount[0]->amount

        );

       }

       else{
        $ds = $this->con->prepare("SELECT * FROM fee_voucher WHERE id='{$_GET['voucher']}'");$ds->execute();
        $res_ds = $ds->fetch();

        $v_discount = $res_ds['voucher_amount'];

        $dsc[] = array(
            'id'        => $_GET['voucher'],
            'amount'    => $v_discount

        );

        

        }//success voucher     
        
        $this->payload[]                    = array(
            'total_tuition_fee'          =>  $this->summary[0]->total_tuition_fee - $v_discount,
            'tuition_fee'                =>  $this->summary[0]->tuition_fee,
            'misc_fee'                   =>  $this->summary[0]->misc_fee,
            'lab_fee'                    =>  $this->summary[0]->lab_fee,
            'discount'                   =>  $this->summary[0]->discount,
            'voucher_discount'           =>  $dsc,
            'tuition_summary'            =>  $this->summary[0]->tuition_summary,
            'tuition_payment_timestamps' =>  $this->summary[0]->tuition_payment_timestamps
          );


          $v = $this->con->prepare("UPDATE enrollment_student SET voucher_id = '{$_GET['voucher']}' WHERE id='{$this->enrollment['id']}'");
          $v->execute();
        
    }

    public function delete_discount()
    {

        $index    = array_search($_GET['delete'],$this->summary[0]->discount,true);
        $transfer = $this->summary[0]->discount[$index]->amount;
        
        unset($this->summary[0]->discount[$index]);

        
        foreach($this->summary[0]->discount as $di){
            $dsc[]= array(
                'id'     => $di->id,
                'amount' => $di->amount
            );

        
     }
      
        $this->payload[]                 =   array(
            'total_tuition_fee'          =>  $this->summary[0]->total_tuition_fee + $transfer,
            'tuition_fee'                =>  $this->summary[0]->tuition_fee,
            'misc_fee'                   =>  $this->summary[0]->misc_fee,
            'lab_fee'                    =>  $this->summary[0]->lab_fee,
            'discount'                   =>  $dsc === null ? array() : $dsc,
            'voucher_discount'           =>  $this->summary[0]->voucher_discount,
            'tuition_summary'            =>  $this->summary[0]->tuition_summary,
            'tuition_payment_timestamps' =>  $this->summary[0]->tuition_payment_timestamps
          );          
            //insert discount
        $idis=$this->con->prepare("DELETE FROM student_discounts WHERE enrollment_student_id ='{$this->enrollment['id']}' && discount_id = '{$_GET['delete']}'");
        $idis->execute();
    
    }
    public function delete_voucher()
    {

        $index    = array_search($_GET['vdelete'],$this->summary[0]->voucher_discount,true);
        $transfer = $this->summary[0]->voucher_discount[$index]->amount;
        
        unset($this->summary[0]->voucher_discount[$index]);
               
        
        $this->payload[]                 =   array(
            'total_tuition_fee'          =>  $this->summary[0]->total_tuition_fee + $transfer,
            'tuition_fee'                =>  $this->summary[0]->tuition_fee,
            'misc_fee'                   =>  $this->summary[0]->misc_fee,
            'lab_fee'                    =>  $this->summary[0]->lab_fee,
            'discount'                   =>  $this->summary[0]->discount,
            'voucher_discount'           =>  $this->summary[0]->voucher_discount,
            'tuition_summary'            =>  $this->summary[0]->tuition_summary,
            'tuition_payment_timestamps' =>  $this->summary[0]->tuition_payment_timestamps
          );

          
        //insert discount
        // $idis=$this->con->prepare("DELETE FROM student_discounts WHERE enrollment_student_id ='{$this->enrollment['id']}' && discount_id = '{$_GET['delete']}'");
        // $idis->execute();
        
        $v = $this->con->prepare("UPDATE enrollment_student SET voucher_id = 0 WHERE id='{$this->enrollment['id']}'");
        $v->execute();


       
    }

    public function update_payment()
    {

        $fees_summary = json_encode($this->payload);
        $this->con->query("UPDATE enrollment_student SET fees_summary ='$fees_summary'  WHERE id='{$this->enrollment['id']}'");

        //ASSESSED
        $this->con->query("UPDATE preregistration_info SET enrollment_status ='ASSESSED' WHERE id='{$_GET['student']}'");


    }
}
    
$data= new Payments($db);
$dat = json_decode(file_get_contents("php://input"));

@$typ = $dat->type;

    $data->enrollment();
    $data->select();

// $data->delete_discount();
if(isset($_GET['create'])){
    $data->create_payment();
    $data->update_payment();

}
if(isset($_GET['discount'])){
    $data->add_discount();
    $data->update_payment();
}
if(isset($_GET['voucher'])){
    $data->add_voucher();
    $data->update_payment();
}
if(isset($_GET['delete'])){
    $data->delete_discount();
    $data->update_payment();
}
if(isset($_GET['deletev'])){
    $data->delete_voucher();
    $data->update_payment();
}

if(isset($_GET['fetch_course'])){
    $data->select('courses');    
}
else if($typ=='Add School'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                            'name'     => $name,
                            'code'     => $code
                        );
$data->insert('schools',$insert_data);
}
else if($typ=='Add Course'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                                'name'     => $name,
                                'code'     => $code
                            );
$data->insert('courses',$insert_data);

}
else if($typ=='Edit School'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;
    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );
$data->update('schools',$insert_data);
}
else if($typ=='Edit Course'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;
    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );
    $data->update('courses',$insert_data);
}
?>