<?php
require "pdo_db.php";


class Building{

    private $con;
    

    public function __construct($db){

        $this->con = $db;
    }

    public function select($table_name, $data)  
    {  
        

        if($data ==0){
            $command = 1;
        }
        else {
            $command = "building_id = '$data'";
        }

         $objects = array();  
         $query =$this->con->prepare("SELECT * FROM {$table_name} WHERE {$command} ORDER BY id DESC");  
         $query->execute();
         
         while($row = $query->fetch())  
         {  
              $objects[] = $row;  
         }  
         echo json_encode($objects);  
    }  

    public function insert($table_name, $data)  
    {  
         
        $string = "INSERT INTO ".$table_name." (";            
           $string .= implode(",", array_keys($data)) . ') VALUES (';            
           $string .= "'" . implode("','", array_values($data)) . "')";  
           $query=$this->con->prepare($string); 
           
           $query->execute();
    }


    public function update($table_name, $data){

    $up = $this->con->prepare("UPDATE {$table_name} SET  name='{$data['name']}' WHERE id='{$data['id']}'");
    $up->execute();

    // echo $data['id'];

    }
    
    

}
    

$data= new Building($db);
$dat = json_decode(file_get_contents("php://input"));

@$typ = $dat->type;


if(isset($_GET['fetch_building'])){
$data->select('buildings',0);
}

else if(isset($_GET['fetch_room'])){
$data->select('rooms',$_GET['fetch_room']);    
}

else if($typ=='insert'){
    $name = $dat->name;
    $insert_data = array( 
                            'name'     => $name
                        );

$data->insert('buildings',$insert_data);

}
else if($typ=='update'){
    $name = $dat->name;
    $id = $dat->id;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     => $name
                        );

$data->update('buildings',$insert_data);

}
else if($typ=='updateroom'){
    $name = $dat->name;
    $id = $dat->id;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     => $name
                        );

$data->update('rooms',$insert_data);

}

else if($typ=='room'){
    $name = $dat->name;
    $id = $dat->id;
    $insert_data = array( 
                            'name'     => $name,
                            'building_id' =>$id
                        );

$data->insert('rooms',$insert_data);

}
?>