<?php
$valueSelected = $_POST['valueSelected'];
if ($valueSelected == 'SHS'){
  echo '<div class="e-cols" ng-app="regApp" ng-controller="mainController">
  <div class="e-col-12 e-form-group">
     <br>
     <h5 class="e-label">ELEMENTARY</h5>
   </div>
   <div class="e-col-6 e-form-group">
     <label class="e-label">School Name</label>
     <input class="e-control" type="text" placeholder="Text">
   </div>
   <div class="e-col-6 e-form-group">
    <label class="e-label">Month/Yr of Completion</label>
     <input class="e-control" type="date" placeholder="Date">
    </div>
    <div class="e-col-12 e-form-group">
     <label class="e-label">School Address</label>
    <input class="e-control" type="text" placeholder="Text" disabled id="ESchoolAddress">
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">Region</label>
    <select class="e-select width_inherit" ng-click="getRegion()" ng-model="ESreg" id="ESAddress_Region">
      <option value="" disabled="" class="" selected="selected">Select Region</option>
      <option ng-repeat="region in regions" value="{{region.regCode}}">{{region.regDesc}}</option>
    </select>
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">Province</label>
    <select class="e-select width_inherit" ng-click = "getProvince()" ng-model = "ESprov" id="ESAddress_Province" disabled>
      <option value="" disabled="" class="" selected="selected">Select Province</option>
      <option ng-repeat = "province in provinces | filter : ESreg" value = "{{province.provCode}}">{{province.provDesc}}</option>
    </select>
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">City/Municipality</label>
    <select class="e-select width_inherit" ng-click="getCityMunci()" ng-model = "EScitymunc" id="ESAddress_CityMunc" disabled>
      <option value="" disabled="" class="" selected="selected">Select City/Municipality</option>
      <option ng-repeat = "citymunci in citymuncis | filter : ESprov" value = "{{citymunci.citymunCode}}">{{citymunci.citymunDesc}}</option>
    </select>
  </div>
  <div class="e-col-6 e-form-group">
    <label class="e-label">Passer of PEPT for Elementary level?</label>
    <select class="e-select width_inherit">
     <option>Select An Option</option>
     <option>Yes</option>
     <option>No</option>
   </select> </div>
   <div class="e-col-6 e-form-group">
     <label class="e-label">Month/Yr of Completion</label>
     <input class="e-control" type="date" placeholder="Date">
   </div>
   <div class="e-col-6 e-form-group">
     <label class="e-label">Passer of  A&E Test for Elementary level?</label>
     <select class="e-select width_inherit">
       <option>Select An Option</option>
       <option>Yes</option>
       <option>No</option>
     </select> </div>
     <div class="e-col-6 e-form-group">
       <label class="e-label">Month/Yr of Completion</label>
       <input class="e-control" type="date" placeholder="Date">
     </div>

   <div class="e-col-12 e-form-group">
     <br>
      <h5 class="e-label">Junior High School</h5>
   </div>
   <div class="e-col-6 e-form-group">
      <label class="e-label">School
   Name</label>
    <input class="e-control" type="text" placeholder="Text">
  </div>
   <div class="e-col-6 e-form-group">
      <label class="e-label">Month/Yr of Completion</label>
      <input class="e-control" type="date" placeholder="Date">
   </div>
   <div class="e-col-12 e-form-group">
      <label class="e-label">School
   Address</label>
    <input class="e-control" type="text" placeholder="Text"
   disabled id="HSchoolAddress">
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">Region</label>
    <select class="e-select width_inherit" ng-change="getRegion()" ng-model="HSreg" id="HSAddress_Region">
      <option value="" disabled="" class="" selected="selected">Select Region</option>
      <option ng-repeat="region in regions" value="{{region.regCode}}">{{region.regDesc}}</option>
    </select>
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">Province</label>
    <select class="e-select width_inherit" ng-click = "getProvince()" ng-model = "HSprov" id="HSAddress_Province" disabled>
      <option value="" disabled="" class="" selected="selected">Select Province</option>
      <option ng-repeat = "province in provinces | filter : HSreg" value = "{{province.provCode}}">{{province.provDesc}}</option>
    </select>
  </div>
  <div class="e-col-4 e-form-group">
    <label class="e-label">City/Municipality</label>
    <select class="e-select width_inherit" ng-click="getCityMunci()" ng-model = "HScitymunc" id="HSAddress_CityMunc" disabled>
      <option value="" disabled="" class="" selected="selected">Select City/Municipality</option>
      <option ng-repeat = "citymunci in citymuncis | filter : HSprov" value = "{{citymunci.citymunCode}}">{{citymunci.citymunDesc}}</option>
    </select>
  </div>
  <div class="e-col-6 e-form-group">
    <label class="e-label">Passer of PEPT for JHS level?</label>
    <select class="e-select width_inherit">
      <option>Select An Option</option>
      <option>Yes</option>
      <option>No</option>
    </select>
  </div>
  <div class="e-col-6 e-form-group">
    <label class="e-label">Month/Yr of Completion</label>
    <input class="e-control" type="date" placeholder="Date">
  </div>
  <div class="e-col-6 e-form-group">
   <label class="e-label">Passer of  A&E Test for JHS level?</label>
   <select class="e-select width_inherit">
     <option>Select An Option</option>
     <option>Yes</option>
     <option>No</option>
   </select>
  </div>
  <div class="e-col-6 e-form-group">
    <label class="e-label">Month/Yr of Completion</label>
    <input class="e-control" type="date" placeholder="Date"> </div>
  </div>
  </div>';
}

?>
