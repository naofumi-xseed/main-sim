<?php
//native API for shifting student midway sim 2.0

// include "../../_config/db.php";
// $data = json_decode(file_get_contents("php://input"));


// $id          = $data->student;
// $curricula   = $data->cur;
// $strand      = $data->strand;

// // detecting student
// $result = $db->query("SELECT * FROM student_curricula WHERE student_id='$id'");
// $count  = $result->num_rows;


// //if there is a student the previous curricula must be deactivated
// if($count>0){
//     $db->query("UPDATE student_curricula SET active=0 WHERE student_id='$id'");
//     $init = 1;
// }

// //or else the enrollment may fail in inserting curriculum , a shift can always set as curriculum
// else{

//     $db->query("INSERT INTO student_curricula
//     (
//         student_id,
//         curricula_id,
//         active

//     )
//     VALUES
//     (
//         '$id',
//         '$curricula',
//         '1'
//     )

// ");
    
// }
// if($init==1){
//     //insert new curricula and make it active
// $db->query("INSERT INTO student_curricula
//     (
//         student_id,
//         curricula_id,
//         active

//     )
//     VALUES
//     (
//         '$id',
//         '$curricula',
//         '1'
//     )

// ");
// //then updates the prereg info as it needs the latest strand however the previous strand is already recorded
// $db->query("UPDATE preregistration_info SET strand_course='$strand' WHERE id='$id'");
// }

require "pdo_db.php";

class Shift{

    private $con;
    public  $dat;
    // #1 : public  $check;
    public $insert;

        public function __construct($db){
            $this->con = $db;
        }
        public function phpdata(){
            $this->dat = json_decode(file_get_contents("php://input"));
        }
        public function check(){
        // detecting student
           $cur = $this->con->prepare("SELECT * FROM student_curricula WHERE student_id= :id");
           $cur->execute([':id' => $this->dat->student]);

        //  #1 : check for duplicate
        //    $exist = $this->con->prepare("SELECT * FROM student_curricula WHERE student_id= :id && curricula_id = :cur");
        //    $exist->execute(array(
        //        ':id' => $this->dat->student,
        //        ':cur'=> $this->dat->cur
        //     ));
        // Pwede  balikan kaya okay lang kung madoble basta ang mahalaga yung ngayon
            
           
           if($cur->rowCount()>0){
            //  #1 :  if($exist->rowCount()){
                    $this->update();
            //      $this->userMessage(1);
            //    }
           }
           else{
                //or else the enrollment may fail in inserting curriculum , a shift can always set as curriculum
               $this->insert();
           }

        }
        public function insert(){
               //insert new curricula and make it active
           $in = $this->con->prepare("INSERT INTO student_curricula
                        (
                            student_id,
                            curricula_id,
                            active

                        )
                        VALUES
                        (
                            :id,
                            :cur,
                            :stat
                        )

                        ");
           $this->insert = $in->execute(array(
               ':id'   => $this->dat->student,
               ':cur'  => $this->dat->cur,
               ':stat' => 1
           ));
           if($this->insert){
               $this->updateStudent();
           }
            
        }
        public function update(){
            //if there is a student the previous curricula must be deactivated
            $up = $this->con->prepare("UPDATE student_curricula SET active=0 WHERE student_id= :id");
            $up->execute([':id' => $this->dat->student]);
             
            if($up->rowCount()>0){
                   
                        $this->insert();
                   
            }
         }
         public function updateStudent(){
        //then updates the prereg info as it needs the latest strand however the previous strand is already recorded
            $upi= $this->con->prepare("UPDATE preregistration_info SET strand_course= :strand WHERE id= :id");
            $upi->execute(array(
                ':strand' => $this->dat->strand,
                ':id'     => $this->dat->student
            ));

            $upi->rowCount() || $this->insert ? $this->UserMessage(0) : $this->UserMessage(2); //user message check if curricula is successfully inserted 
         
 
         }
         public function userMessage($e){

            $message = array(
                        'Successfully Shifted',
                        'Error Already Shifted',
                        'There is a problem'
                      );
           echo $message[$e];

          
             
         }
}

$data = new Shift($db);
$data->phpdata();
$data->check();
