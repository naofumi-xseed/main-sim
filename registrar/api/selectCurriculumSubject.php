<?php
include "../../_config/db.php";
$output = array();  
$query = $db->query("SELECT curriculum_details.*,subjects.*,terms.*,year_levels.*,year_levels.in_word as year_name,subjects.name as descriptive_title,subject_types.*,subject_types.name as subtype_name, subjects.id as subject_id,curriculum_details.id as curriculum_did,
                    curriculum_details.subject_id as cs_id
                      FROM curriculum_details
                      INNER JOIN subjects ON subjects.id = curriculum_details.subject_id
                      INNER JOIN subject_types ON subjects.subject_type_id = subject_types.id
                      INNER JOIN terms ON terms.id = curriculum_details.term_id
                      INNER JOIN year_levels ON year_levels.id = curriculum_details.year_level_id
                      WHERE curriculum_details.curricula_id='{$_GET['cur_id']}'
                    ");  
     while($row = $query->fetch_assoc())  
     {  
          $output[] = $row;  
     }  
     echo json_encode($output,JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;
?>