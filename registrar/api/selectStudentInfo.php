<?php
require 'pdo_db.php';
require 'Input.php';

class Student{

 private $con;
 public  $dat;

 public $a;
 public $d;
    
    public function __construct($db,$d,$a)
    {
        $this->con = $db;  
        $this->in = $a;
        $this->d = $d;
    }
    public function select()
    {
        $out = array();
        $s = $this->con->prepare("SELECT p.*,psi.* , sc.*,sc.name as strand_name, p.strand_course as strand_id
                                    FROM preregistration_info as p
                                    INNER JOIN preregistration_supporting_info as psi ON psi.student_id = p.id
                                    LEFT OUTER JOIN strands_courses as sc ON sc.id = p.strand_course
                                    WHERE p.id = :id
        "); 

        $s->execute([':id'=>$_GET['id']]);

        $r = $s->fetch();
        // $out[] = $r;
        echo json_encode($r, JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL; 
    }
    public function upload()
    {

    }
    public function edit() //$params
    {

         // This is use to handle more accurately javascript array encode and decode again 
         $je = json_encode($this->in[0]['data']);
         $jd = json_decode($je);
         //
            $editing = $this->con->prepare("UPDATE preregistration_info as p
            INNER JOIN preregistration_supporting_info as  psi ON p.id = psi.student_id
             SET 

                p.email           ='$jd->email',
                p.contact_no      ='$jd->contact_no',
                p.birthday        ='$jd->birthday',
                p.sex             ='$jd->sex',
                p.first_name      ='$jd->first_name',
                p.middle_name     ='$jd->middle_name',
                p.last_name       ='$jd->last_name',
                p.birth_place     ='$jd->birth_place',
                psi.civil_status  ='$jd->civil_status',
                psi.religion      ='$jd->religion',
                psi.mother_tongue ='$jd->mother_tongue'
                
                WHERE p.id        = '{$this->d->id}'
            ");
            
            $editing->execute();

            if($editing->rowCount()>0){
                echo "updated";
            }
            
    }
}
    $data = new Student($db,$d,$a);

    if(isset($_GET['id']))
    {
        $data->select();
    }

if(isset($_GET['edit'])){
    $data->edit();
}

