<?php
require 'pdo_db.php';

class Prospectus{

private $con;

    public function __construct($db)
    {
        $this->con = $db;
    }
    public function select()
    {

        $records = array();
        $query = $this->con->prepare("SELECT DISTINCT cd.*,t.*,yl.* ,yl.in_word as year_name,yl.id as y_id 
                                        FROM  curriculum_details as cd
                                        INNER JOIN terms as t ON t.id = cd.term_id 
                                        INNER JOIN year_levels as yl ON yl.id = cd.year_level_id
                                        WHERE cd.curricula_id='{$_GET['cur_id']}' group by cd.year_level_id
                                    ");$query->execute();
 
            while($cd = $query->fetch())  
            {  
                $counter = count($records);

                $records[$counter] = array(
                        'y_id'          => $cd['y_id'],
                        'year_name'     => $cd['year_name'],
                        'terms'     => array()
                );


                $term = $this->con->prepare("SELECT DISTINCT cd.*,t.*,yl.* ,yl.in_word as year_name , t.in_word as term_name,t.id as t_id, cd.id as c_id
                FROM  curriculum_details as cd
                INNER JOIN terms as t ON t.id = cd.term_id 
                INNER JOIN year_levels as yl ON yl.id = cd.year_level_id
                WHERE cd.curricula_id='{$_GET['cur_id']}' group by cd.term_id
            ");$term->execute();

                while($tr = $term->fetch())
                {
                    $counter2 = count($records[$counter]['terms']);
                    $records[$counter]['terms'][] = array(
                        'c_id'           => $tr['c_id'],
                        't_id'           => $tr['t_id'],
                        'term_name'      => $tr['term_name'],
                        'subjects'  => array()
                    );

                    $subjects = $this->con->prepare("SELECT cd.*,s.*,t.*,yl.*,yl.in_word as year_name,s.name as descriptive_title,st.*,st.name as subtype_name, s.id as subject_id,cd.id as curriculum_did,
                    cd.subject_id as cs_id,s.id as sub_id
                      FROM curriculum_details as cd
                      INNER JOIN subjects as s ON s.id = cd.subject_id
                      INNER JOIN subject_types as st ON s.subject_type_id = st.id
                      INNER JOIN terms  as t ON t.id = cd.term_id
                      INNER JOIN year_levels as yl ON yl.id = cd.year_level_id
                      WHERE cd.curricula_id='{$_GET['cur_id']}' && cd.term_id = '{$tr['term_id']}' && cd.year_level_id = '{$cd['year_level_id']}'
                    ");$subjects->execute(); 

                        while($sub = $subjects->fetch())
                        {

                            $records[$counter]['terms'][$counter2]['subjects'][] = array(
                                'sub_id'            => $sub['sub_id'],
                                'p_id'              => $sub['subject_parent_id'],
                                'code'              => $sub['code'],
                                'descriptive_title' => $sub['descriptive_title'],
                                'subtype_name'      => $sub['subtype_name'],
                                'units'             => $sub['units'],
                                'lecture_hours'     => $sub['lecture_hours'],
                                'lab_hours'         => $sub['lab_hours'],
                                'weeks'             => $sub['weeks'],
                                'total_hours'       => $sub['weeks']*$sub['lecture_hours']
                            );

                        } //third
                }//second 

                
            }  //initial loop
            
            echo json_encode($records);//output
    }//select function




}//class

$data = new Prospectus($db);
$data->select();