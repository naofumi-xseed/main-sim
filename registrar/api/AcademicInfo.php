<?php
require "pdo_db.php";

class Academic {

    private $con;

        public function __construct($db){
            $this->con = $db;
        }

        public function select()
        {

                $output = array();
                $acads = $this->con->prepare("SELECT ss.*,s.* FROM student_subjects as ss
                                            INNER JOIN subjects as s ON s.id = ss.subject_id
                                            WHERE  ss.student_id = :stud && ss.setting_id= :setting
                                            ");
                $acads->execute(array(
                                ':stud'    => $_GET['id'],
                                ':setting' => $_GET['set']
                                ));
                                
                while($result = $acads->fetch()){
                    $output[] = $result;
                }
                echo json_encode($output);

                if(isset($_GET['gen'])){
                    $this->generate($output);
                }
               
        }
        public function values(){

            $output = array();

            $core = array(
                            "D",
                            "R",
                            "I",
                            "V",
                            "E",
                            "N"
                        );

            

            $values = $this->con->prepare("SELECT sv.*,vr.name as rname ,v.name as vname FROM student_values as sv
                                            LEFT OUTER JOIN value_remarks as vr ON vr.id = sv.value_remark_id
                                            LEFT OUTER JOIN vals as v ON v.id = sv.value_id
                                            WHERE sv.student_id = :stud && sv.setting_id = :setting
                                         ");
             $values->execute(array(
                ':stud'    => $_GET['id'],
                ':setting' => $_GET['set']
                ));
            
            foreach($core as $c)
            {

                $result = $values->fetch();
                $output[] = array(
                    'desc' =>$c,
                    'vname' =>$result['vname'],
                    'rname' =>$result['rname']

                );
            }
           echo  json_encode($output);

        //    echo count($core);

        }

        public function generate($datas){

           $random = array(50,50,45,100);

           // $random = array(1.00,6.22,6.20,6.50);
            $glen = count($random)-1;

            foreach($datas as $d){
            $rgrade = rand(0,$glen);
            $gen = $this->con->prepare("UPDATE student_subjects set final='{$random[$rgrade]}'
                                        WHERE subject_id = '{$d['subject_id']}' && student_id ='{$_GET['id']}' && setting_id='{$_GET['set']}'
                                     ");

            $gen->execute();
            }

        }
}
$data = new Academic($db);

if(isset($_GET['find'])){
    $data->select();
}

if(isset($_GET['value'])){
    $data->values();
}


// if(isset($_GET['gen'])){
//     $data->generate();
// }
