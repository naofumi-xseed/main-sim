<?php


// include "../../_config/db.php";



// $schedules = array();
// $schedule_details = array();


// //setting
// $s = $db->query("SELECT id,name,strand_id,year_level_id,setting_id FROM schedules WHERE id='{$_GET['id']}'");

// while($out_s = $s->fetch_assoc()){

//     $schedule_index = count($schedules);

//     $schedules[$schedule_index] = array(
//         'id' => $out_s['id'],
//         'name' => $out_s['name'],
//         'details' => array()   
//     );


//     if(isset($_GET['subj'])){
//         $query = "schedule_details.schedule_id='{$out_s['id']}' && schedule_details.subject_id = '{$_GET['subj']}'";
//     }
//     else{
//         $query = "schedule_details.schedule_id='{$out_s['id']}'";
//     }

// $sd = $db->query("SELECT schedule_details.*,subjects.*,schedule_details.id as sched_id,subjects.id as subj_id
//                     FROM schedule_details
//                     INNER JOIN subjects ON schedule_details.subject_id = subjects.id
//                     WHERE {$query}
//                 ");

//         while($out_sd = $sd->fetch_assoc()){

//             $sd_index = count($schedules[$schedule_index]['details']);
//             $schedules[$schedule_index]['details'][] = array(

//                 'id'         => $out_sd['sched_id'],
//                 'subject_id' => $out_sd['subj_id'],
//                 'code'       => $out_sd['code'],
//                 'name'       => $out_sd['name'],
//                 'hours'      => $out_sd['weeks']*$out_sd['lecture_hours'],
//                 'units'      => $out_sd['units'],
//                 'schedule_subject' => array()
//             );

//                 $ssd = $db->query("SELECT d.short_name,t.first_name,t.last_name,sd.*,r.name as room,b.name as building
//                                                   FROM schedule_subject_details as sd
//                                                   LEFT JOIN days as d on d.id = sd.day_id
//                                                   INNER JOIN teachers as t on sd.teacher_id = t.id
//                                                   INNER JOIN rooms as r on sd.room_id=r.id
//                                                   INNER JOIN buildings as b on r.building_id=b.id
//                                                   WHERE sd.schedule_detail_id ='{$out_sd['sched_id']}'
//                            ");
//                            while($out_ssd = $ssd->fetch_assoc()){
                            
//                             $schedules[$schedule_index]['details'][$sd_index]['schedule_subject'][] = array(

//                                     'day'        => $out_ssd['short_name'],
//                                     'start_time' => $out_ssd['start_time'],
//                                     'end_time'   => $out_ssd['end_time'],
//                                     'room'       => $out_ssd['room'],
//                                     'building'   => $out_ssd['building'],
//                                     'teacher'    => $out_ssd['first_name'].$out_ssd['last_name']
//                             );


//                            }


//         }

    
    
// }
// echo json_encode($schedules);


?>

<?php
//native API for enrollment of subject midway sim 2.0
require "pdo_db.php";





class ScheduleSubject{

    private $con;

        public function __construct($db){
            $this->con = $db;
        }

        public function select(){
            $schedules = array();
            $s = $this->con->prepare("SELECT id,name,strand_id,year_level_id,setting_id FROM schedules WHERE id='{$_GET['id']}'");
            $s->execute();

                while($out_s = $s->fetch()){

                    $schedule_index = count($schedules);

                    $schedules[$schedule_index] = array(
                        'id' => $out_s['id'],
                        'name' => $out_s['name'],
                        'details' => array()   
                    );


                    if(isset($_GET['subj'])){
                        $query = "sd.schedule_id='{$out_s['id']}' && sd.subject_id = '{$_GET['subj']}'";
                    }
                    else{
                        $query = "sd.schedule_id='{$out_s['id']}'";
                    }

                $sd = $this->con->prepare("SELECT sd.*,s.*,sd.id as sched_id,s.id as subj_id
                                            FROM schedule_details as sd
                                            INNER JOIN subjects as s ON sd.subject_id = s.id
                                            WHERE {$query}
                                        ");
                 $sd->execute();

                        while($out_sd = $sd->fetch()){

                            $sd_index = count($schedules[$schedule_index]['details']);
                            $schedules[$schedule_index]['details'][] = array(

                                'id'         => $out_sd['sched_id'],
                                'subject_id' => $out_sd['subj_id'],
                                'code'       => $out_sd['code'],
                                'name'       => $out_sd['name'],
                                'hours'      => $out_sd['weeks']*$out_sd['lecture_hours'],
                                'units'      => $out_sd['units'],
                                'schedule_subject' => array()
                            );

                                $ssd = $this->con->prepare("SELECT d.short_name,t.first_name,t.last_name,sd.*,r.name as room,b.name as building
                                                                FROM schedule_subject_details as sd
                                                                LEFT JOIN days as d on d.id = sd.day_id
                                                                INNER JOIN teachers as t on sd.teacher_id = t.id
                                                                INNER JOIN rooms as r on sd.room_id=r.id
                                                                INNER JOIN buildings as b on r.building_id=b.id
                                                                WHERE sd.schedule_detail_id ='{$out_sd['sched_id']}'
                                        ");
                                $ssd->execute();
                                        while($out_ssd = $ssd->fetch()){
                                            
                                            $schedules[$schedule_index]['details'][$sd_index]['schedule_subject'][] = array(

                                                    'day'        => $out_ssd['short_name'],
                                                    'start_time' => $out_ssd['start_time'],
                                                    'end_time'   => $out_ssd['end_time'],
                                                    'room'       => $out_ssd['room'],
                                                    'building'   => $out_ssd['building'],
                                                    'teacher'    => $out_ssd['first_name'].$out_ssd['last_name']
                                            );


                                        }


                        }

                    
                    
                }
                echo json_encode($schedules);
            

        }
        
}

$data = new ScheduleSubject($db);

$data->select();