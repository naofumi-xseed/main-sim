<?php
require "pdo_db.php";


class Schools{

    private $con;

    public function __construct($db){
        $this->con=$db;

    }
    
    public function select($table_name)  
    {  
         
         $objects = array();  
         $stmt =$this->con->prepare("SELECT * FROM {$table_name} ORDER BY id DESC");  
         $stmt->execute();
         while($row = $stmt->fetch())  
         {  
              $objects[] = $row;  
         }  
         echo json_encode($objects,JSON_INVALID_UTF8_IGNORE). PHP_EOL . PHP_EOL;  
    }  

    public function insert($table_name, $data)  
    {  
         
           $stmt  =$this->con->prepare("INSERT INTO {$table_name} (code,name) VALUES (:code,:name)");           
           
           

           $stmt->execute(array( 
               ':name' => $data['name'], 
               ':code' => $data['code']
           ));
         
           
    }


    public function update($table_name, $data){

    $up =$this->con->prepare("UPDATE {$table_name} SET  name='{$data['name']}', code ='{$data['code']}' WHERE id='{$data['id']}'");
    $up->execute();
    mysqli_query($this->con,$up);

    

    }
    
    

}
    

$data= new Schools($db);
$dat = json_decode(file_get_contents("php://input"));

@$typ = $dat->type;


if(isset($_GET['fetch_school'])){
$data->select('schools');
}

if(isset($_GET['fetch_course'])){
$data->select('courses');    
}

else if($typ=='Add School'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                            'name'     => $name,
                            'code'     => $code
                        );

$data->insert('schools',$insert_data);

}

else if($typ=='Add Course'){
    $name = $dat->name;
    $code = $dat->code;
    $insert_data = array( 
                            'name'     => $name,
                            'code'     => $code
                        );

$data->insert('courses',$insert_data);

}

else if($typ=='Edit School'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );

$data->update('schools',$insert_data);

}

else if($typ=='Edit Course'){
    $name = $dat->name;
    $id   = $dat->id;
    $code = $dat->code;

    $insert_data = array( 
                            'id'       =>$id,
                            'name'     =>$name,
                            'code'     =>$code
                        );

$data->update('courses',$insert_data);

}

?>