<?php

/*
Input class for this system


d - data
a - array

*/


Class Input{

    public $dat;
    public $arr;

        public function phpdata(){
            $this->dat = json_decode(file_get_contents("php://input"));
           return $this->dat;
          
        }

        public function phparray(){
            $jsonText = file_get_contents('php://input');
            $decodedText = html_entity_decode($jsonText);
            $this->arr = json_decode('[' . $decodedText . ']', true);

            return $this->arr;

        }
}


$input = new Input();

$d = $input->phpdata();
$a = $input->phparray();

