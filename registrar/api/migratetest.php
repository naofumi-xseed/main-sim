<?php
require "pdo_db.php";

class Migrate{

    private $con;

        public function __construct($db){
            $this->con = $db;
        }

        public function Make(){

            $s = $this->con->prepare("SELECT DISTINCT s.*,ss.*,et.* FROM enrollment_transactions as et
                    LEFT OUTER JOIN students as s ON et.student_id=s.id 
                    LEFT OUTER JOIN strands as ss ON et.student_id = ss.id
                    WHERE et.status='Enrolled'  group by et.student_id
                  ");
            $s->execute();

            $st=$this->con->prepare("INSERT INTO student_table 
                (
                    first_name,
                    last_name,
                    middle_name,
                    student_no,
                    strand
                )
                VALUES
                (
                    :fname,
                    :lname,
                    :mname,
                    :sno,
                    :stra
                )
                
                ");

            while($r=$s->fetch()){

                $st->execute(
                    array(
                            ':fname' => $r['first_name'],
                            ':lname' => $r['last_name'],
                            ':mname' => $r['middle_name'],
                            ':sno'   => $r['student_no'],
                            ':stra'  => $r['code'] === null ? 'None' : $r['code']
                        )
                );

            }

            echo"success";

        }

}

$data = new Migrate($db);
$data->Make();

