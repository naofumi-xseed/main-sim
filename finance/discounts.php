<div class="e-container">
  <p class="text-bold">Discounts</p>
  <div class="align-end">
      <select class="e-select" id="semester_option">
        <option value="" disabled="" class="" selected="selected">Select Semester</option>
        <?php
        $sqlsem = "SELECT settings.id as settings_id, settings.description as description FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
        $resultsem = $db ->query($sqlsem);
        while($rowsem = $resultsem->fetch_assoc()){
          ?>
          <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
        <?php } ?>
      </select>
  </div>

    <table class="e-table bordered hovered mt-3" id="indextable">

    </table>



    <div id="NoResults" hidden="hidden">
    <br>
    <br>
    <center>
    <h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>
    </center>
    </div>
    <div id="loading-image" hidden="hidden">
      <center>
        <img src="../_public/photos/loader.gif" style="width:600px;height:auto;">
      </center>
    </div>
<script>
$('#semester_option').on('change', function (e) {
    var optionSelected = $("option:selected", this).text();
    var valueSelected = this.value;
    $('#indextable').hide();
    $('#NoResults').hide();
    $('#loading-image').show();
    $.ajax({
      type:"POST",
      data: "valueSelected="+valueSelected,
      url:"api/load_discounts.php",
      success:function(data){
        if (data==1){
          $('#indextable').hide();
          $('#NoResults1').html("No Results found in ");
          $('#NoResults2').text(optionSelected);
          $('#NoResults').show();
        }
        else{
        $('#NoResults').hide();
        $('#indextable').show();
        $('#indextable').html(data);
        }
      },
      complete: function(){
        $('#loading-image').hide();
      }
    });

});

function deleteDiscount(disID){
  $.ajax({
    type:"POST",
    data: "disID="+disID,
    url:"api/delete_discount.php",
    success:function(data){
      alert('Deleted');
    }
  });
}

</script>
