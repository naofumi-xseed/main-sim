<div class="e-container">
  <p class="text-bold pl-3">Fees</p>
  <div class="e-cols">
    <div class="e-col-8">
      <table class="e-table bordered hovered" id="indextable">
        <thead class="e-thead primary">
          <tr>
            <th><a href="javascript:SortTable(0,'N');" >PRIORITY</a></th>
            <th><a href="javascript:SortTable(1,'T');" >CODE</a></th>
            <th><a href="javascript:SortTable(2,'T');" >NAME</a></th>
            <th><a href="javascript:SortTable(3,'T');" >FEE TYPE</a></th>
            <th>ACTION</th>
          </tr>
        </thead>
        <tbody id="tbody_fee">
          <?php
          include "../_config/db.php";
          $sql = "SELECT fees.id as fees_id, fees.priority as priority, fees.code as code, fees.name as name, fee_type.name as fee_type FROM fees INNER JOIN fee_type on fees.fee_type_id = fee_type.id ORDER BY fee_type.id";
          $result = $db ->query($sql);

          while($row = $result->fetch_assoc()){
            ?>
            <tr>
              <td><?php echo $row['priority'];?></td>
              <td style="text-transform: uppercase;"><?php echo $row['code'];?></td>
              <td style="text-transform: uppercase;"><?php echo $row['name'];?></td>
              <td style="text-transform: uppercase;"><?php echo $row['fee_type'];?></td>
              <td>
                <div class="centered between">
                  <a onclick= "updateFee(<?php echo $row['fees_id'];?>)"><i class="fa fa-edit text-sky"></i></a>
                  <a onclick= "deleteFee(<?php echo $row['fees_id'];?>)"><i class="fa fa-trash-alt text-danger"></i></a>
                </div>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    <div id="replaceForm" class="e-col-4">
      <!-- Create New Fee -->
      <div id="CreateFee">
        <form class="e-form-box">

          <h3 class="text-gray centered pb-1">Create New Fee</h3>

          <div class="e-form-group unified">
            <div class="e-control-helper text-primary">
              Priority
            </div>
            <input  type="number" class="e-control" name="priority" id="priority">
          </div>

          <div class="e-form-group unified">
            <div class="e-control-helper text-primary">
              Code
            </div>
            <input  type="text" class="e-control text-uppercase" name="code" id="code">
          </div>

          <div class="e-form-group unified">
            <div class="e-control-helper text-primary">
              Name
            </div>
            <input type="text" class="e-control text-uppercase" name="name" id="name">
          </div>

          <div class="e-form-group unified">
            <div class="e-control-helper text-primary">
              Type
            </div>
            <select class="e-select" name="fee_type" id="fee_type">
              <option value="" disabled="" class="" selected="selected">Select Fee Type</option>
              <option label="TUITION FEE" value="1">TUITION FEE</option>
              <option label="MISCELLANEOUS FEE" value="2">MISCELLANEOUS FEE</option>
              <option label="BACK ACCOUNT" value="3">BACK ACCOUNT</option>
            </select>
          </div>

          <div class="between">
            <button type="button" class="e-btn inverted dark width-40" onClick="CreateNew();" >Create New</button>
            <button type="button" class="e-btn primary width-40" onClick="addFee();">Save</button>
          </div>

        </form>
      </div>

    </div>
  </div>
  <!-- End of Create new Fee -->

</div>

<script>

function addFee() {
  var priority = $('#priority').val();
  var code = $('#code').val();
  var name = $('#name').val();
  var fee_type = $('#fee_type').val();

  $.ajax({
    type:'POST',
    url: 'api/add_fee.php',
    data:"priority="+priority+"&code="+code+"&name="+name+"&fee_type="+fee_type,
    success:function(data){
      console.log('success');
      loadFee();
      alert('Added Fee');
    },
    error: function(data){
      console.log('error');
      console.log(data);
    }
  });
}

function editFee() {
  var priority = $('#priority').val();
  var code = $('#code').val();
  var name = $('#name').val();
  var id = $('#id').val();
  var fee_type = $('#fee_type').val();

  $.ajax({
    type:'POST',
    url: 'api/edit_fee.php',
    data:"priority="+priority+"&code="+code+"&name="+name+"&id="+id+"&fee_type="+fee_type,
    success:function(data){
      console.log('success');
      loadFee();
      AlertSuccess();
    },
    error: function(data){
      console.log('error');
      console.log(data);
    }
  });
}

function updateFee(fees_id){
  var fees_id = fees_id;
  $.ajax({
    type:'POST',
    url: 'api/update_fee_form.php',
    data:"fees_id="+ fees_id,
    success:function(data){
      $('#replaceForm').html(data);
      console.log('success');
    }
  });
}

function deleteFee(strID){
  var id = strID;

  $.ajax({
    type:"GET",
    url:"api/delete_fee.php",
    data:"id="+id,
    success:function(data){
      loadFee();
      alert('Fee Deleted!');

    }

  });

}

function loadFee(){
  $.ajax({
    type:"GET",
    url:"api/load_fee.php",
    success:function(data){
      $('#tbody_fee').html(data);
    }
  });
}

function CreateNew(){
  $('#replaceForm').html('<!-- Create New Fee --> <div id="CreateFee"> <form class="e-form-box"> <h3 class="text-gray centered pb-1">Create New Fee</h3> <div class="e-form-group unified"> <div class="e-control-helper text-primary"> Priority </div> <input  type="number" class="e-control" name="priority" id="priority"> </div> <div class="e-form-group unified"> <div class="e-control-helper text-primary"> Code </div> <input  type="text" class="e-control text-uppercase" name="code" id="code"> </div> <div class="e-form-group unified"> <div class="e-control-helper text-primary"> Name </div> <input type="text" class="e-control text-uppercase" name="name" id="name"> </div> <div class="e-form-group unified"> <div class="e-control-helper text-primary"> Type </div> <select class="e-select" name="fee_type" id="fee_type"> <option value="" disabled="" class="" selected="selected">Select Fee Type</option> <option label="TUITION FEE" value="1">TUITION FEE</option> <option label="MISCELLANEOUS FEE" value="2">MISCELLANEOUS FEE</option> <option label="BACK ACCOUNT" value="3">BACK ACCOUNT</option> </select> </div> <div class="between"> <button type="button" class="e-btn inverted dark width-40" onClick="CreateNew();" >Create New</button> <button type="button" class="e-btn primary width-40" onClick="addFee();">Save</button> </div> </form> </div> </div> </div> <!-- End of Create new Fee -->');
}

function AlertSuccess(){
  $('#successAdded')
 .fadeIn( function()
 {
    setTimeout( function()
    {
       $("#successAdded").fadeOut('2000');
    }, 3000);
 });
}

$('.arrow').on('click',function()
{
  var valAttr = $(this).children('span').attr( "class" );
  if(valAttr == "arrow-up"){
    $(this).children('span').removeClass('arrow-up');
    $(this).children('span').addClass('arrow-down');
  }
  else if(valAttr == "arrow-down"){
    $(this).children('span').removeClass('arrow-down');
    $(this).children('span').addClass('arrow-up');
  }
});

</script>
