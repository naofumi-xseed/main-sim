<div class="e-container">
  <p class="text-bold">Settings</p>
    <table class="e-table bordered hovered" cellspacing="0" style="margin-bottom:50px;" id="indextable">
      <thead class="e-thead primary">
        <tr>
          <th>DESCRIPTION</th>
          <th>STATUS</th>
        </tr>
      </thead>
      <tbody>

        <?php
        include "../_config/db.php";
        $sql = "SELECT settings.id as settings_id, settings.description as description, settings.status as status,school_years.year, terms.id FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
        $result = $db ->query($sql);

        while($row = $result->fetch_assoc()){
          ?>
          <tr>
            <td><a href = "?show_setting=<?php echo $row['settings_id'];?>" class="text-primary"><?php echo $row['description'];?></a></td>
            <td style='text-transform: uppercase;' class="checkActive"><?php echo $row['status'];?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
</div>
<script>
$(function() {
 $(".checkActive").each(function() {
  var valAttr = $(this).html();
  if(valAttr == '0'){
    $(this).html("NOT ACTIVE");
  }
  else if(valAttr == '1'){
    $(this).html("ACTIVE");
      }
    });
});

</script>
