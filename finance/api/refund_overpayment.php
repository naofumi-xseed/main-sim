<?php
include "../../_config/db.php";
$enrollID= $_POST['enrollID'];
$IDUser = $_POST['IDUser'];
$overpayment = $_POST['overpayment'];
$checkno = $_POST['checkno'];
$refund = abs($_POST['overpayment']);

$date = new DateTime('now', new DateTimeZone('Asia/Manila'));
$datenow = $date->format('Y-m-d H:i:s');

$query="INSERT INTO `student_refunds`(`enrollment_student_id`, `user_id`, `check_no`, `amount`, `created_at`) VALUES ('$enrollID','$IDUser','$checkno','$refund','$datenow')";
$result=$db->query($query);

$sqlReceipt =  "SELECT receipt_no FROM `student_payment_transactions` WHERE enrollment_student_id = '$enrollID'";
$resultReceipt = $db->query($sqlReceipt);

$receiptNo = $resultReceipt->fetch_assoc()['receipt_no'];

$payload = ["fee_id" => "30",
            "code" => "RFD",
            "description" => "REFUND",
            "amount" => "$overpayment",
            "is_posted" => "1",
            "is_voided" => "0"];
$fees_pay_transac = json_encode($payload);
$queryRefund="INSERT INTO `student_payment_transactions`(`enrollment_student_id`, `amount`, `receipt_no`, `transaction_details`, `cashier`, `created_at`) VALUES ('$enrollID','$overpayment','$receiptNo','$fees_pay_transac','ADMIN','$datenow')";

$resultRefund=$db->query($queryRefund);
?>
