<?php
include "../../_config/db.php";
$totalrows =  $db->query('SELECT COUNT("*") FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id')->fetch_assoc()['COUNT("*")'];

$limit = 20;

$pages = ceil($totalrows / $limit);

$page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
  'options' => array(
    'default'   => 1,
    'min_range' => 1,
  ),
)));

$offset = ($page - 1)  * $limit;

$start = $offset + 1;
$end = min(($offset + $limit), $totalrows);
$data = "SELECT preregistration_info.id as id, preregistration_info.student_number as student_no, preregistration_info.first_name as first_name, preregistration_info.middle_name as middle_name, preregistration_info.last_name as last_name, preregistration_info.reference_no as reference_no, strands_courses.name as strand_name  FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON enrollment_student.strand_id = strands_courses.id where preregistration_info.id > 0  LIMIT {$limit} OFFSET {$offset}";

$connect = mysqli_query($db,$data);

while($results=mysqli_fetch_array($connect)){
  $is_active = 0;
  $sql = "SELECT t.is_active as is_active FROM student_blocking t INNER JOIN ( SELECT student_id, max(created_at) as MaxDate FROM student_blocking WHERE student_id = {$results['id']} AND department_id = 7 GROUP BY student_id ) tm ON t.student_id = tm.student_id AND t.created_at = tm.MaxDate WHERE t.student_id = {$results['id']} AND t.department_id = 7";
  $result = $db ->query($sql);
  $rowcount=mysqli_num_rows($result);
  echo '<tr>';
  echo '<td><a href = "?student_info='.$results['id'].'" class="text-primary">'.$results['student_no'].'</a></td>';
  echo '<td>'.$results['reference_no'].'</td>';
  echo '<td style=\'text-transform: uppercase;\'>'.$results['last_name'].', '.$results['first_name'].' '.$results['middle_name'].'</td>';
  echo '<td style=\'text-transform: uppercase;\'>';
  echo '<div class="centered">';
  if($rowcount==0){
    echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$results['id'].')">Block</a>';
  }else{
    while($row = $result->fetch_assoc()){
      $is_active = $row['is_active'];
      if ($is_active == 1){
        echo '<a onclick="unblockStudent('.$results['id'].')" class="e-btn rounded sky width-60">Unblock</a>';
      }else if ($is_active == 0){
        echo '<a class="e-btn rounded danger width-60" onclick="ShowBlockModal('.$results['id'].')">Block</a>';
      }
    }
  }
  echo '</div>';
  echo '</td>';
  echo '</tr>';
}
?>
