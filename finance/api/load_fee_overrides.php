<?php
include "../../_config/db.php";
$valueSelected = $_POST['valueSelected'];
$sql = "SELECT students.id as student_id, student_fee_overrides.id as overrides_id, students.student_no as stud_no, students.last_name as last_name, students.first_name as first_name, students.middle_name as middle_name, fees.name as fee_name, student_fee_overrides.amount as amount, students.reference_no as LRN  FROM `student_fee_overrides` LEFT OUTER JOIN fees ON student_fee_overrides.fee_id = fees.id LEFT OUTER JOIN settings ON student_fee_overrides.id = settings.id LEFT OUTER JOIN students ON student_fee_overrides.student_id = students.id WHERE student_fee_overrides.setting_id = $valueSelected";
$result = $db ->query($sql);
$rowcount=mysqli_num_rows($result);
if ($rowcount==0){
 echo '1';
}
else{
while($row = $result->fetch_assoc()){
echo  '<tr>';
echo  '<td hidden class="override_id">'.$row['overrides_id'].'</td>';
echo  '<td><a href = "?student_info='.$row['student_id'].'" class="text-primary">'.$row['stud_no'].'</a></td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$row['LRN'].'</td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$row['last_name'].', '.$row['first_name'].' '.$row['middle_name'].'</td>';
echo  '<td style=\'text-transform: uppercase;\'>'.$row['fee_name'].'</td>';
echo  '<td style=\'text-transform: uppercase;\' class="override_amount">'.$row['amount'].'</td>';
echo  '<td style=\'text-transform: uppercase;\' class="override_edit" hidden>'.$row['amount'].'</td>';
echo  '<td class="group1_buttons">';
echo  '<div class="centered between">';
echo  '<a onclick="editAmount(this)"><i class="fa fa-edit text-sky"></i></a>';
echo  '<a onclick= "deleteOverride('.$row['overrides_id'].')"><i class="fa fa-trash-alt text-danger"></i></a>';
echo  '</div>';
echo  '</td>';
echo  '<td class="group2_buttons" hidden style="padding-top:15px;">';
echo  '<div class="centered between">';
echo  '<a onclick="saveEdit(this)"><i class="fa fa-check text-sky"></i></a>';
echo  '<a onclick="cancelEdit(this)"><i class="fa fa-times text-danger"></i></span></a>';
echo  '</div>';
echo  '</td>';
echo  '</tr>';
 }
} ?>
