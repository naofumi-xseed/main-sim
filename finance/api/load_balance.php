<?php
include "../../_config/db.php";
$yearLevelID = $_POST['yearLevelValue'];
$strandValue = $_POST['strandValue'];
$settingValue = $_POST['settingValue'];
$TotalTotal = 0;
$sqlMain = "SELECT enrollment_student.id as enrollment_id, enrollment_student.voucher_id, enrollment_student.fees_summary, preregistration_info.id as student_id, preregistration_info.student_number, preregistration_info.first_name, preregistration_info.middle_name, preregistration_info.last_name, strands_courses.code as strand_code FROM `preregistration_info` LEFT OUTER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON strands_courses.id = enrollment_student.strand_id where enrollment_student.strand_id = '$strandValue' AND enrollment_student.year_level_id = '$yearLevelID' AND enrollment_student.setting_id = '$settingValue'";
$resultMain = $db->query($sqlMain);
while($rowSTUDdetails = $resultMain->fetch_assoc()){
  $TotalTuition = 0;
  $TotalPayment = 0;
  $TuitionFee = 0;
  $MiscFee = 0;
  $LabFee = 0;
  $VoucherDiscount = 0;
  $TotalDiscount = 0;

  $enrollment_id = $rowSTUDdetails['enrollment_id'];
  $voucher_id = $rowSTUDdetails['voucher_id'];
  $student_id = $rowSTUDdetails['student_id'];
  $student_number = $rowSTUDdetails['student_number'];
  $first_name = $rowSTUDdetails['first_name'];
  $middle_name = $rowSTUDdetails['middle_name'];
  $last_name = $rowSTUDdetails['last_name'];
  $strand_code  = $rowSTUDdetails['strand_code'];
  $fees_summary  = $rowSTUDdetails['fees_summary'];


  $sqlTransactionPayment =  "SELECT sum(amount) as amount FROM `student_payment_transactions` WHERE enrollment_student_id = '$enrollment_id'";
  $resultTransactionPayment = $db->query($sqlTransactionPayment);

  $TotalPayment = $resultTransactionPayment->fetch_assoc()['amount'];

  $someJSON = '['.$fees_summary.']';
  $someArray = json_decode($someJSON, true);
  foreach ($someArray as $key => $value) {
     $TotalTuition = $value["total_tuition_fee"];
     $TuitionFee = $value["tuition_fee"];
     $MiscFee = $value["misc_fee"];
     $LabFee = $value["lab_fee"];
     $VoucherDiscount = $value["voucher_discount"];
     $TotalDiscount = $value["discount"];
      }

(int) $TotalBalance = $TotalTuition - $TotalPayment;

if($TotalBalance > 0){
    echo '<tr>';
    echo '<td><a href = "?student_info='.$student_id.'" class="text-primary">'.$student_number.'</a></td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$last_name.', '.$first_name.' '.$middle_name.'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$strand_code.'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TuitionFee), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($MiscFee), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($LabFee), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TotalTuition), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($VoucherDiscount), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TotalDiscount), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TotalPayment), 2, ".", ",").'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.number_format(($TotalBalance), 2, ".", ",").'</td>';
    echo '</tr>';
    $TotalTotal+=$TotalBalance;
}
}

  echo  '<tr>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td>Total:</td>';
  echo  '<td>'.number_format(($TotalTotal), 2, ".", ",").'</td>';
  echo  '</tr>';

?>
