<?php
include "../../_config/db.php";
$studentID=$_POST['studentID'];
$settingIDSelected=$_POST['settingIDSelected'];
$settingIDString=$_POST['settingIDString'];

$sqlSTUDdetails = "SELECT enrollment_student.id as enrollment_id, preregistration_info.student_number, preregistration_info.first_name, preregistration_info.middle_name, preregistration_info.last_name, enrollment_student.year_level_id, enrollment_student.strand_id, enrollment_student.setting_id, enrollment_student.voucher_id, strands_courses.name as strand_name FROM  `preregistration_info` LEFT OUTER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON strands_courses.id = enrollment_student.strand_id where preregistration_info.id = '$studentID' AND enrollment_student.setting_id = '$settingIDSelected'";
$resultSTUDdetails = $db->query($sqlSTUDdetails);
$rowcountSTUDdetails = mysqli_num_rows($resultSTUDdetails);
if($rowcountSTUDdetails == 0) {
  echo '<div id="NoResults">
  <br>
  <br>
  <center>
  <h3 id="NoResults1">No Results found <h3 id="NoResults2">'.$settingIDString.'</h3></h3>
  </center>
  </div>';
}else{
while($rowSTUDdetails = $resultSTUDdetails->fetch_assoc()){
  $enrollment_id = $rowSTUDdetails['enrollment_id'];
  $student_number = $rowSTUDdetails['student_number'];
  $first_name = $rowSTUDdetails['first_name'];
  $middle_name = $rowSTUDdetails['middle_name'];
  $last_name = $rowSTUDdetails['last_name'];
  $year_level_id = $rowSTUDdetails['year_level_id'];
  $strand_id  = $rowSTUDdetails['strand_id'];
  $setting_id  = $rowSTUDdetails['setting_id'];
  $strand_name  = $rowSTUDdetails['strand_name'];
  $voucher_id  = $rowSTUDdetails['voucher_id'];
  }

$sqlLAB = "SELECT DISTINCT subjects.code as subject_code, subjects.name as subject_name, fee_subjects.subject_amount FROM student_subjects LEFT OUTER JOIN subjects ON subjects.id = student_subjects.subject_id LEFT OUTER JOIN fee_subjects ON student_subjects.subject_id = fee_subjects.subject_id WHERE student_subjects.enrollment_id = '$enrollment_id' AND fee_subjects.strand_id = '$strand_id' AND fee_subjects.setting_id = '$settingIDSelected'";
$resultLAB = $db->query($sqlLAB);
$rowcount=mysqli_num_rows($resultLAB);
if($rowcount == 0) {
echo '<div id="NoResults">
<br>
<br>
<center>
<h3 id="NoResults1">No Results found <h3 id="NoResults2">'.$settingIDString.'</h3></h3>
</center>
</div>';
}else{

  $sqlfee = "SELECT fees.name as fee_name, fee_structure.fee_amount  FROM `fee_structure` LEFT OUTER JOIN fees ON fee_structure.fee_id = fees.id WHERE strand_id  = $strand_id AND  year_level_id = '$year_level_id' AND setting_id = '$setting_id' AND fee_amount_type = 1";
  $resultfee = $db->query($sqlfee);

  $sqlMISC = "SELECT sum(fee_amount) as total_misc FROM `fee_structure` WHERE strand_id  = '$strand_id' AND  year_level_id = '$year_level_id' AND setting_id = '$setting_id'";
  $resultMISC = $db->query($sqlMISC);

  $sqlLABTotal =  "SELECT DISTINCT fee_subjects.subject_amount as lab_amount FROM student_subjects LEFT OUTER JOIN fee_subjects ON student_subjects.subject_id = fee_subjects.subject_id WHERE student_subjects.student_id = '$studentID' AND fee_subjects.strand_id = '$strand_id' AND fee_subjects.setting_id = '$setting_id'";
  $resultLABTotal = $db->query($sqlLABTotal);

   $sqlBackAcc =  "SELECT `amount` FROM `student_back_accounts` WHERE enrollment_student_id = '$enrollment_id'";
   $resultBackAcc = $db->query($sqlBackAcc);

  $sqlDiscount =  "SELECT discounts.code, discounts.description, discounts.amount, discounts.fee_amount_type FROM `student_discounts` LEFT OUTER JOIN discounts ON student_discounts.discount_id = discounts.id WHERE enrollment_student_id = '$enrollment_id'";
  $resultDiscount = $db->query($sqlDiscount);

  $sqlVoucher = "SELECT vouchers.description, fee_voucher.voucher_amount FROM `fee_voucher` LEFT OUTER JOIN vouchers ON fee_voucher.voucher_id = vouchers.id WHERE `setting_id` = '$setting_id' AND voucher_id = '$voucher_id'";
  $resultVoucher = $db->query($sqlVoucher);

  $sqlTransaction =  "SELECT `amount`, `receipt_no`, `transaction_details`, `cashier`, `created_at`, `updated_at` FROM `student_payment_transactions` WHERE enrollment_student_id = '$enrollment_id'";
  $resultTransaction = $db->query($sqlTransaction);

  $sqlTransactionPayment =  "SELECT sum(amount) as amount FROM `student_payment_transactions` WHERE enrollment_student_id = '$enrollment_id'";
  $resultTransactionPayment = $db->query($sqlTransactionPayment);

  $sqlUnits = "SELECT SUM(subjects.units) as sum_units FROM `student_subjects` LEFT OUTER JOIN subjects ON student_subjects.subject_id = subjects.id WHERE student_subjects.student_id = '$studentID' AND student_subjects.setting_id = '$setting_id'";
  $resultUnits = $db->query($sqlUnits);

  $sqlTuitionRate = "SELECT fee_structure.fee_amount as tuition_rate FROM `fee_structure` LEFT OUTER JOIN fees ON fee_structure.fee_id = fees.id WHERE strand_id  = '$strand_id' AND  year_level_id = '$year_level_id' AND setting_id = '$setting_id' AND fees.id = 1";
  $resultTuitionRate = $db->query($sqlTuitionRate);

  $sqlLecHours = "SELECT sum(subjects.lec_hours * subjects.weeks) as sum_hours FROM `student_subjects` LEFT OUTER JOIN subjects ON student_subjects.subject_id = subjects.id WHERE student_subjects.student_id = '$studentID' AND student_subjects.setting_id = '$setting_id'";
  $resultLecHours = $db->query($sqlLecHours);

  $sqlExam = "SELECT DISTINCT fees.name as exam_fee, fee_structure.fee_amount as exam_per_subject FROM `fee_structure` LEFT OUTER JOIN fees ON fee_structure.fee_id = fees.id WHERE strand_id  = '$strand_id' AND  year_level_id = '$year_level_id' AND setting_id = '$setting_id' AND fees.id=21";
  $resultExam = $db->query($sqlExam);

  $sqlCollegeSubjects = "SELECT student_subjects.subject_id, subjects.name as subject_name FROM `student_subjects` LEFT OUTER JOIN subjects ON student_subjects.subject_id = subjects.id WHERE student_subjects.student_id = '$studentID' AND student_subjects.setting_id = '$setting_id' AND subjects.units !=0";
  $resultCollegeSubjects = $db->query($sqlCollegeSubjects);

  $sqlSHSubjects = "SELECT student_subjects.subject_id, subjects.name as subject_name, subjects.lab_hours, subjects.lecture_hours as lec_hours, subjects.is_academic, subjects.weeks FROM `student_subjects` LEFT OUTER JOIN subjects ON student_subjects.subject_id = subjects.id WHERE student_subjects.student_id = '$studentID' AND student_subjects.setting_id = '$setting_id' ";
  $resultSHSubjects = $db->query($sqlSHSubjects);

  $TotalLAB = 0;
  $Totalfee = 0;
  $TotalTuition = 0;
  $BackAdd = 0;
  $TotalBotAmount = 0;
  $TotalStudentFee = 0;
  $TotalExam = 0;
  $TotalMISC = 0;
  $TotalDiscount = 0;

while($rowLABTotal = $resultLABTotal->fetch_assoc()){
  $TotalLAB += $rowLABTotal['lab_amount'];
}
  $rowcountSHSubjects=mysqli_num_rows($resultSHSubjects);
  $rowcountCollegeSubjects=mysqli_num_rows($resultCollegeSubjects);
  if($rowcountCollegeSubjects ==0 || $rowcountSHSubjects==0){

  }else{
  while($rowExam = $resultExam->fetch_assoc()){
    $ExamName = $rowExam['exam_fee'];
    $ExamRate = $rowExam['exam_per_subject'];
  }
  if($year_level_id == '1' || $year_level_id == '2'){
    $TotalExam = $ExamRate*$rowcountSHSSubjects;
  }else{
    $TotalExam = $ExamRate*$rowcountCollegeSubjects;
    }

  }

  $TotalMISC = $resultMISC->fetch_assoc()['total_misc'];

  $Totalfee =$TotalExam+$TotalMISC;
  $TuitionRate = $resultTuitionRate->fetch_assoc()['tuition_rate'];
  if($year_level_id == '1' || $year_level_id == '2'){
    $TotalLecHours = 0;
    $TotalLecHours = $resultLecHours->fetch_assoc()['sum_hours'];
    $TotalTuition = $TotalLecHours*$TuitionRate;
  }else{
    $TotalUnits=0;
    $TotalUnits = $resultUnits->fetch_assoc()['sum_units'];
    $TotalTuition = $TotalUnits*$TuitionRate;
  }

  $rowcountDiscount=mysqli_num_rows($resultDiscount);
  if($year_level_id == '1' || $year_level_id == '2'){
    if($rowcountDiscount == 0) {
      while($rowVoucher = $resultVoucher->fetch_assoc()){
        $DiscountName = $rowVoucher['description'];
        $TotalDiscount = $rowVoucher['voucher_amount'];
      }
    } else{
        $TotalDiscount=0;
        while($rowDiscount = $resultDiscount->fetch_assoc()){
          $DiscountName = $rowDiscount['description'];
          if($rowDiscount['fee_amount_type'] !== '1'){
            $TotalDiscount = $rowDiscount['amount'] * $TotalTuition;
          }else{
          $TotalDiscount = $rowDiscount['amount'];
        }
  }
  }

}else{
      $TotalDiscount=0;
      while($rowDiscount = $resultDiscount->fetch_assoc()){
        $DiscountName = $rowDiscount['description'];
        if($rowDiscount['fee_amount_type'] !== '1'){
          $TotalDiscount = $rowDiscount['amount'] * $TotalTuition;
        }else{
        $TotalDiscount = $rowDiscount['amount'];
      }
    }
}

  $rowcountBackAcc=mysqli_num_rows($resultBackAcc);
  if($rowcountBackAcc == 0) {
  }else{
    while($rowBackAcc = $resultBackAcc->fetch_assoc()){
      $BackAdd = $rowBackAcc['amount'];
    }
  }

  $TotalBotAmount = $resultTransactionPayment->fetch_assoc()['amount'];

  $SubTotal = $Totalfee+$TotalTuition+$TotalLAB-$TotalDiscount;
  // $SubTotal = $Totalfee+$TotalTuition+$TotalLAB;
  $TotalStudentFee = $SubTotal+$BackAdd;

  echo  '<div class="e-cols">';
  echo  '<div class="e-col-6">';
  echo  '<div class="e-box">';

  echo  '<h4 class="text-uppercase">'.$last_name.', '.$first_name.' '.$middle_name.'</h4>';
  echo  '<h4>'.$student_number.'</h4>';
  echo  '<h4 class="text-uppercase">'.$strand_name.'</h4>';

  echo  '<div class="align-end">';
  echo  '<h6>TUITION FEE: ₱ '.number_format(($TotalStudentFee), 2, ".", ",").'</h6>';
  echo  '</div>';
  echo  '<div class="align-end">';
  echo  '<h6>TOTAL PAYMENT: ₱ '.number_format(($TotalBotAmount), 2, ".", ",").'</h6>';
  echo  '</div>';
  echo  '<div class="align-end">';
  echo  '<h6>BALANCE: ₱ '.number_format(($TotalStudentFee - $TotalBotAmount), 2, ".", ",").'</h6>';
  echo  '</div>';
  echo  '</div>';
  echo  '</div>';
  echo  '<div class="e-col-6">';
  echo  '<div class="e-box">';
  echo  '<table class="e-table" id="indextable">';
  echo  '<thead class="e-thead">';
  echo  '<tr>';
  echo  '<th><a href="javascript:SortTable(0,\'N\');" >DESCRIPTION</a></th>';
  echo  '<th class="align-end"><a href="javascript:SortTable(1,\'T\');" >AMOUNT</a></th>';
  echo  '</tr>';
  echo  '</thead>';
  echo  '<tbody id="tbody_fee">';

  echo  '<tr class="bg-primary">';
  echo  '<td class="text-white">MISCELLANEOUS</th>';
  echo  '<td class="text-white align-end">₱ '.number_format(($Totalfee), 2, ".", ",").'</td>';
  echo  '</tr>';
  while($rowfee = $resultfee->fetch_assoc()){
    echo  '<tr>';
    echo  '<td class="no-border no-p" style="text-transform: uppercase;">'.$rowfee['fee_name'].'</td>';
    echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($rowfee['fee_amount']), 2, ".", ",").'</td>';
    echo  '</tr>';
  }

  $rowcountExam=mysqli_num_rows($resultExam);
  if($rowcountExam == 0) {
  }
  else{
  echo  '<tr>';
  echo  '<td class="no-border no-p" style="text-transform: uppercase;">'.$ExamName.'</td>';
  echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($TotalExam), 2, ".", ",").'</td>';
  echo  '</tr>';
  }
  echo  '<tr class="bg-primary">';
  echo  '<td class="text-white">TUITION</th>';
  echo  '<td class="text-white align-end">₱ '.number_format(($TotalTuition), 2, ".", ",").'</td>';
  echo  '</tr>';
  echo  '<tr>';
  echo  '<tr>';
  echo  '<td class="no-border no-p" style="text-transform: uppercase;">TUITION FEE</td>';
  echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($TotalTuition), 2, ".", ",").'</td>';
  echo  '</tr>';

  echo  '<tr class="bg-primary">';
  echo  '<td class="text-white">LABORATORY</th>';
  echo  '<td class="text-white align-end">₱ '.number_format(($TotalLAB), 2, ".", ",").'</td>';
  echo  '</tr>';
  while($rowLAB = $resultLAB->fetch_assoc()){
    echo  '<tr>';
    echo  '<td class="no-border no-p" style="text-transform: uppercase;">'.$rowLAB['subject_code'].'</td>';
    echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($rowLAB['subject_amount']), 2, ".", ",").'</td>';
    echo  '</tr>';
  }


  if($TotalDiscount  == '0.00' || $TotalDiscount  == '0'){

  }else{
    echo  '<tr class="bg-primary">';
    echo  '<td class="text-white">DISCOUNT</th>';
    echo  '<td class="text-white align-end">(₱ '.number_format(($TotalDiscount), 2, ".", ",").')</td>';
    echo  '</tr>';
    echo  '<tr>';
    echo  '<td class="no-border no-p" style="text-transform: uppercase;">'.$DiscountName.'</td>';
    echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">(₱ '.number_format(($TotalDiscount), 2, ".", ",").')</td>';
    echo  '</tr>';
  }

  echo  '<tr>';
  echo  '<td class="no-p" style="text-transform: uppercase;border-ti">SUBTOTAL: </td>';
  echo  '<td class="no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($SubTotal), 2, ".", ",").'</td>';
  echo  '</tr>';

  echo  '<tr>';
  echo  '<td class="no-border no-p" style="text-transform: uppercase;border-ti">BACK ACCOUNT: </td>';
  echo  '<td class="no-border no-p align-end" style="text-transform: uppercase;">₱ '.$BackAdd.'</td>';
  echo  '</tr>';


  echo  '<tr>';
  echo  '<td class="no-p" style="text-transform: uppercase;border-ti">TOTAL: </td>';
  echo  '<td class="no-p align-end" style="text-transform: uppercase;">₱ '.number_format(($TotalStudentFee), 2, ".", ",").'</td>';
  echo  '</tr>';

  echo  '</tbody>';
  echo  '</table>';
  echo  '</div>';
  echo  '</div>';
  echo  '</div>';
  echo  '<div class="e-cols">';
  echo  '<div class="e-col-12">';
  echo  '<div class="e-box">';
  echo  '<table class="e-table" id="indextable">';
  echo  '<thead class="e-thead">';
  echo  '<tr>';
  echo  '<th><a href="javascript:SortTable(0,\'N\');" >CODE</a></th>';
  echo  '<th><a href="javascript:SortTable(1,\'T\');" >DESCRIPTION</a></th>';
  echo  '<th><a href="javascript:SortTable(2,\'T\');" >AMOUNT</a></th>';
  echo  '<th><a href="javascript:SortTable(3,\'T\');" >OR #</a></th>';
  echo  '<th><a href="javascript:SortTable(4,\'T\');" >DATE RECEIVED</a></th>';
  echo  '<th><a href="javascript:SortTable(5,\'T\');" >DATE POSTED</a></th>';
  echo  '<th><a href="javascript:SortTable(7,\'T\');" >CASHIER</a></th>';
  echo  '</tr>';
  echo  '</thead>';
  echo  '<tbody>';

  while($rowTransaction = $resultTransaction->fetch_assoc()){
    echo  '<tr>';
    $someJSON = '['.$rowTransaction['transaction_details'].']';
    $someArray = json_decode($someJSON, true);
    foreach ($someArray as $key => $value) {
    echo  '<td class="" style="text-transform: uppercase;">'.$value["code"].'</td>';
    echo  '<td class="" style="text-transform: uppercase;">'.$value["description"].'</td>';
        }
    echo  '<td class="" style="text-transform: uppercase;">₱ '.number_format(($rowTransaction['amount']), 2, ".", ",").'</td>';
    echo  '<td class="" style="text-transform: uppercase;">'.$rowTransaction['receipt_no'].'</td>';
    echo  '<td class="" style="text-transform: uppercase;">'.$rowTransaction['created_at'].'</td>';
    echo  '<td class="" style="text-transform: uppercase;">'.$rowTransaction['updated_at'].'</td>';
    echo  '<td class="" style="text-transform: uppercase;">'.$rowTransaction['cashier'].'</td>';
    echo  '</tr>';
  }
  echo  '<tr>';
  echo  '<td></td>';
  echo  '<td>TOTAL</td>';
  echo  '<td class="" style="text-transform: uppercase;">₱ '.number_format(($TotalBotAmount), 2, ".", ",").'</td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '<td></td>';
  echo  '</tr>';
  echo  '</tbody>';
  echo  '</table>';
  echo  '</div>';
  echo  '</div>';
  echo  '</div>';
}
}

?>
