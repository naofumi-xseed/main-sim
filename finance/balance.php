<div class="e-container mt-3">
  <div id="divBalance">
    <div class="e-cols">
      <div class="e-col-4">

        <h3>Balance</h3>

      </div>
      <div class="e-col">
        <div class="e-form-group unified">
          <div class="e-control-helper marked">
            Select
          </div>
          <select class="e-control text-capitalize" id="strand_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Strand</option>
            <?php
            $sql = "SELECT DISTINCT id, name FROM strands_courses";
            $result = $db ->query($sql);
            while($row = $result->fetch_assoc()){
              ?>
              <option class="text-capitalize" value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="semester_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Semester</option>
            <?php
            $sqlsem = "SELECT settings.id as settings_id, settings.description as description, settings.status as status,school_years.year, terms.id FROM `settings` INNER JOIN school_years ON settings.school_year_id = school_years.id INNER JOIN terms ON settings.term_id = terms.id ORDER BY school_years.year DESC ,`terms`.`id` DESC";
            $resultsem = $db ->query($sqlsem);
            while($rowsem = $resultsem->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowsem['settings_id'];?>"><?php echo $rowsem['description'];?></option>
            <?php } ?>
          </select>
          <select class="e-select" id="yrlvl_option_balance">
            <option value="" disabled="" class="" selected="selected">Select Year</option>
            <?php
            $sqlyear = "SELECT id, short_name FROM year_levels";
            $resultyear = $db ->query($sqlyear);
            while($rowyear = $resultyear->fetch_assoc()){
              ?>
              <option value ="<?php echo $rowyear['id'];?>"><?php echo $rowyear['short_name'];?></option>
            <?php } ?>
          </select>
          <button type="button" class="e-btn primary" id="search_balance" onclick="tableBalance()">Go</button>
        </div>
      </div>
    </div>
  </div>


  <table class="e-table bordered hovered" id="indextableOverpayment">
  <thead>
  <tr>
  <th><a href="javascript:SortTable(0,\'N\');">Student No.</a></th>
  <th><a href="javascript:SortTable(1,\'T\');"><b>Name</b></a></th>
  <th><a href="javascript:SortTable(2,\'T\');"><b>Course/Strand</b></a></th>
  <th><a href="javascript:SortTable(2,\'N\');"><b>Tuition Fee</b></a></th>
  <th><a href="javascript:SortTable(3,\'N\');"><b>Misc Fee</b></a></th>
  <th><a href="javascript:SortTable(4,\'N\');"><b>Lab Fee</b></a></th>
  <th><a href="javascript:SortTable(5,\'N\');"><b>Total School Fees</b></a></th>
  <th><a href="javascript:SortTable(6,\'N\');"><b>Voucher</b></a></th>
  <th><a href="javascript:SortTable(7,\'N\');"><b>Discount</b></a></th>
  <th><a href="javascript:SortTable(8,\'N\');"><b>Paid Amount</b></a></th>
  <th><a href="javascript:SortTable(9,\'N\');"><b>Balance</b></a></th>
  </tr>
  </thead>
  <tbody id="tbody">

  </tbody>
  </table>
  <div id="NoResults" hidden="hidden">
  <br>
  <br>
  <center>
  <h3 id="NoResults1">No Results found <h3 id="NoResults2"></h3></h3>
  </center>
  </div>
  <div id="loading-image" hidden="hidden">
  <center>
  <img src="../_public/photos/loader.gif" style="width:600px;height:auto;">
  </center>
  </div>


<!-- Block Modal -->
<div class="e-modal" id="blockModal">
  <div class="e-modal-content eUp">
    <header class="e-modal-header">
      <p class="e-modal-title">Remarks for blocking</p>
      <button type="button" class="e-delete close">
        <i aria-hidden="true">&times;</i>
      </button>
    </header>
    <div class="e-modal-body">
      <form class="e-cols no-gap">
        <div class="e-col e-form-group">
          <input class="e-control" type="text" id="remarks" placeholder="Remarks">
          <input class="e-control" type="text" id="studID" style="display:none;">
        </div>
      </form>
    </div>
    <footer class="e-modal-footer">
      <button class="e-btn danger fullwidth" type="button" onClick="blockStudent()">Block</button>
    </footer>
  </div>
</div>
<!-- End Block Modal -->

<script>

function tableBalance(){
  var yearLevelValue = $("#yrlvl_option_balance").val();
  var strandValue = $("#strand_option_balance").val();
  var settingValue = $("#semester_option_balance").val();
  $('#tbody_balance').hide();
  $('#NoResults').hide();
  $('#loading-image').show();
  $.ajax({
    type: "POST",
    url:  "api/load_balance.php",
    data: "yearLevelValue="+yearLevelValue+"&strandValue="+strandValue+"&settingValue="+settingValue,
    success:function(data) {
      if (data==1){
        $('#NoResults').html("No Results found");
        $('#NoResults').show();
        $('#tbody').hide();
      }
      else{
      $('#NoResults').hide();
      $('#tbody').html(data);
      $('#tbody').show();
      }
    },
    complete: function(){
      $('#loading-image').hide();
    }
  });
}



  </script>
