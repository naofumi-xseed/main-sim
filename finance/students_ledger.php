<div class="e-container">
  <p class="text-bold">Students Ledger</p>
  <div class="e-cols">
    <div class="e-col-8"></div>
    <div class="e-col-4">
      <form>
        <div class="e-form-group unified">
          <div class="e-control-helper">
            <i class="fas fa-search text-primary"></i>
          </div>
          <input class="e-control" id="search" type="text" placeholder="Search...">
          <button type="button" class="e-btn primary" onclick="loadSearch()">Search</button>
        </div>
      </form>
    </div>
  </div>


  <?php
  $totalrows =  $db->query('SELECT COUNT("*") FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id')->fetch_assoc()['COUNT("*")'];

  $limit = 20;

  $pages = ceil($totalrows / $limit);

  $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
    'options' => array(
      'default'   => 1,
      'min_range' => 1,
    ),
  )));

  $offset = ($page - 1)  * $limit;

  $start = $offset + 1;
  $end = min(($offset + $limit), $totalrows);

  echo '<table class="e-table bordered hovered" id="indextable">';
  echo '<thead>';
  $data = "SELECT preregistration_info.id as id, preregistration_info.student_number as student_no, preregistration_info.first_name as first_name, preregistration_info.middle_name as middle_name, preregistration_info.last_name as last_name, preregistration_info.reference_no as reference_no, strands_courses.name as strand_name  FROM preregistration_info INNER JOIN enrollment_student ON preregistration_info.id = enrollment_student.student_id LEFT OUTER JOIN strands_courses ON enrollment_student.strand_id = strands_courses.id where preregistration_info.id > 0  LIMIT {$limit} OFFSET {$offset}";

  echo '<tr>';
  echo '<th><a href="javascript:SortTable(0,\'N\');">Student No.</a></th>';
  echo '<th><a href="javascript:SortTable(1,\'N\');"><b>LRN</b></a></th>';
  echo '<th><a href="javascript:SortTable(2,\'T\');"><b>Name</b></a></th>';
  echo '<th><a href="javascript:SortTable(3,\'T\');"><b>STRAND</b></a></th>';
  echo '</tr>';
  echo '</thead>';
  echo '<tbody id="tbody_students">';

  $connect = mysqli_query($db,$data);

  while($results=mysqli_fetch_array($connect)){

    $qrcode=$results['student_no'].$results['last_name'];

    echo '<tr>';
    echo '<td><a href = "?student_info='.$results['id'].'" class="text-primary">'.$results['student_no'].'</a></td>';
    echo '<td>'.$results['reference_no'].'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$results['last_name'].', '.$results['first_name'].' '.$results['middle_name'].'</td>';
    echo '<td style=\'text-transform: uppercase;\'>'.$results['strand_name'].'</td>';
    echo '</tr>';

    if(isset($_GET['hash']))
    {
      $_SESSION['code']=$_GET['hash'];
    }
  }
  echo '</tbody>';
  echo '</table>';

  echo '<nav class="e-pagination" aria-label="pagination">';
  echo '<div class="e-page-content">';

  $prevlink = ($page > 1) ? '<a href="?students_ledger&page=1" title="First page" class="e-page-item"><i class="fas fa-angle-double-left"></i> </a> <a href="?students_ledger&page=' . ($page - 1) . '" class="e-page-item" title="Previous page">previous</a>' : '<a class="e-page-item" disabled><i class="fas fa-angle-double-left"></i></a> <a class="e-page-item" disabled>previous</a>';

  $nextlink = ($page < $pages) ? '<a href="?students_ledger&page=' . ($page + 1) . '" class="e-page-item" title="Next page">Next</a> <a href="?students_ledger&page=' . $pages . '" class="e-page-item" title="Last page"><i class="fas fa-angle-double-right"></i></a>' : '<span class="e-page-item" disabled>Next</span> <span class="e-page-item" disabled><i class="fas fa-angle-double-right"></i></span>';

  echo  $prevlink, ' Page ', $page, ' of ', $pages, ' pages, displaying ', $start, '-', $end, ' of ', $totalrows, ' results ',     $nextlink;
  echo '</div> </nav> </div>';

  ?>

  <script>
  function loadSearch(){
    var searchValue = $("#search").val();
    $.ajax({
      type: "POST",
      url:"api/search_students_ledger.php",
      data:"searchValue="+searchValue,
      success:function(data){
        $('#tbody_students').html(data);
      }
    });
  }
  $('.arrow').on('click',function()
  {
    var valAttr = $(this).children('span').attr( "class" );
    if(valAttr == "arrow-up"){
      $(this).children('span').removeClass('arrow-up');
      $(this).children('span').addClass('arrow-down');
    }
    else if(valAttr == "arrow-down"){
      $(this).children('span').removeClass('arrow-down');
      $(this).children('span').addClass('arrow-up');
    }
  });
  </script>
