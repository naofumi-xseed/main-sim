<?php

include "_config/db.php";
session_start();


?>

<?php
$time = date('Y-m-d H:i:s');

    
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="_public/css/midway-sim.css">

    <!-- css dependencies -->
    <link rel="stylesheet" href="_public/css/midway-sim.css">
    <link rel="stylesheet" href="_public/css/googlefont_oswald.css">
    <link rel="stylesheet" href="_public/css/bootstrap4.min.css">
    <link rel="stylesheet" href="_public/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="_public/css/font-awesome.min.css">

    <!--JQUERIES DEPENDENCIES-->
    <script src="_public/js/jquery.min.js"></script>
    <script src="_public/js/jquery-3.3.1.slim.min.js"></script>
    <script src="_public/js/popper.min.js"></script>
    <script src="_public/js/4.3.1_bootstrap.min.js"></script>


    <title>SIM</title>
</head>

<body id="body-cont">
    <nav>
    <div class="header main_header py-2">
                <h1 class="pull-left">Midway Colleges</h1>
            </div>
    </nav>

    <div class="container py-4" >


 

        <div class="row py-5">

            <div class="col-md-6 py-5">
                <h3>
                    <img width=70; height=70; src="_public/photos/registrar.png" alt="">
                    <a href="?registrar">
                        Admission& Records Office
                    </a>
                </h3>
            </div>
            <div class="col-md-6 py-5">
                <h3><img width=70; height=70; src="_public/photos/inventory.png" alt="">
                Assets Management
            </h3>
            </div>
            <div class="col-md-6 py-5">
                <h3>
                    <img width=70; height=70; src="_public/photos/money.png" alt="">
                    <a href="?finance">
                        Finance Office
                    </a>
                </h3>
            </div>
            <div class="col-md-6 py-5">
                <h3>
                    <img width=70; height=70; src="_public/photos/acad.png" alt="">
                    <a>
                        Academic Office
                    </a>
                </h3>
            </div>
            <div class="col-md-6 py-5">
                <h3>
                    <img width=70; height=70; src="_public/photos/sickbay.png" alt="">
                    <a>
                        Health and Safety
                    </a>
                </h3>
            </div>

        </div>
        

        

    <div class="modal fade" id="financeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content col-sm-9">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Login</h3>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="finance-tab" data-toggle="tab" href="#finance" role="tab"
                                aria-controls="Finance" aria-selected="true">Finance</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="payco-tab" data-toggle="tab" href="#payco" role="tab"
                                aria-controls="Payco" aria-selected="false">Payco</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="finance" role="tabpanel" aria-labelledby="finance-tab">
                            <div class="form-group">
                                <form action="" method="POST">
                                </br>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="username" class="form-control" placeholder="Password"
                                                    name="financeUsername">
                                            </div>
                                        </div>
                                        </br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="password" class="form-control" placeholder="Password"
                                                    name="financePassword">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                <input type='submit' id="rel" class="btn btn-success" value='Login' name='finance_login'>
                                </form>
                            </div>
                           
                       </div>

                        <div class="tab-pane fade" id="payco" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="form-group">
                                <form action="" method="POST">
                                    <div class="container">
                                    </br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" placeholder="Payco Username"
                                                    name="payco_username">
                                            </div>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <input type="password" class="form-control" placeholder="Password"
                                                    name="payco_password">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                <input type='submit' id="" class="btn btn-success" value='login' name='payco_login'></form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--Login Modal-->



    <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content col-sm-9">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Login</h3>
                </div>
                <div class="modal-body">

                    <div class="form-group">

                        <form action="" method="POST">

                            <div class="container">

                                <div class="row">
                                    <div class="col-sm-4">
                                         <input type="text" class="form-control" placeholder="Username" name="user">
                                        </div>
                                </div><br>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" placeholder="Password" name="pass">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        <input type='submit' id="" class="btn btn-success" value='Login' name='login'></form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php


if(isset($_POST['login'])){

    $username= $_POST['user'];
    $passw= $_POST['pass'];
    if(empty($username) || empty($passw)){

    echo "<br><center><a style='color:red'>Username and password is required</a></center>";
    }

else{

    $hash = md5($passw);

    $users = "SELECT password,employee_no,remember_token from users where employee_no='$username' AND password='$hash'";
    $connect_user =mysqli_query($db,$users);
    $checker = mysqli_num_rows($connect_user);
    $results=mysqli_fetch_array($connect_user);

            if($checker>0 && $results['remember_token']!=1){

            $up_data = " UPDATE users SET remember_token='0', updated_at='$time'  where employee_no='$username'";
            mysqli_query($db,$up_data);

            $_SESSION['username']=$username;
            echo"
            <script>
            window.location ='registrar';
            </script>
            ";
            }
            else if($results['remember_token']==1){

            echo "<br><center><a style='color:red'>You are already login.</a></center>";
            }

                    else{
                    echo "<br><center><a style='color:red'>Your username or password is wrong, please try again later. Thank you.</a></center>";
                    }
            
            }
    }


    
if(isset($_POST['payco_login'])){
   


    $username = $_POST['payco_username'];
    $passw    = $_POST['payco_password'];

    if(empty($username) || empty($passw)){
    echo "<br><center><a style='color:red'>Username and password is required</a></center>";
    }
    else{

    $users = "SELECT password from payco_users where username='$username'";
    $connect_user = $db->query($users);
        while($result = $connect_user->fetch_assoc()){

            if(password_verify($passw,$result['password'])){

                $_SESSION['username']=$username;
                $_SESSION['auth']="authorized";
                echo "
                <script>
                window.location ='payco';
                </script>
                ";
            }else{
                echo "<br><center><a style='color:red'>Your username or password is wrong, please try again later. Thank you.</a></center>";

            }
            
        }
        
    }
}



        
if(isset($_POST['finance_login'])){

    $username=$_POST['financeUsername'];
    $passw=$_POST['financePassword'];
    if(empty($username) || empty($passw)){
    echo "<br><center><a style='color:red'>Username and password is required</a></center>";
    }

else{

    $hash = md5($passw);

    $users = "SELECT password,employee_no,remember_token from users where employee_no='$username' AND password='$hash'";
    $connect_user =mysqli_query($db,$users);
    $checker = mysqli_num_rows($connect_user);

    $results =mysqli_fetch_array($connect_user);

    if($checker>0 && $results['remember_token']!=1)
    {

    $up_data = " UPDATE users SET remember_token='1', updated_at='$time'   where employee_no='$username'";
    mysqli_query($db,$up_data);

    $_SESSION['username']=$username;
    echo "
    <script>
    window.location ='finance';
    </script>
    ";
    }
    else if($results['remember_token']==1){
    echo "<br><center><a style='color:red'>You are already login.</a></center>";
    }
    else{
    echo "<br><center><a style='color:red'>Your username or password is wrong, please try again later. Thank you.</a></center>";
    }
    }
    }

?>


    <!--Login Modal-->
    <script>
        $('#myTab li:first-child a').tab('show');
        $('#myTab a').on('click', function (e) {
            e.preventDefault()
            $(this).tab('show');
        })

    </script>


    <?php


if(isset($_GET['registrar'])){
echo"<script type='text/javascript'>



$('#loginmodal').modal('toggle');
</script>";
}
if(isset($_GET['finance'])){
    echo"<script type='text/javascript'>
    
    
    
    $('#financeModal').modal('toggle');
    </script>";

    
}
?>

    <script src="/_public/js/angular.this.js"></script>
    <script src="controller/app.js"></script>

</body>
</html>
